export interface UserProfile {
    id: number;
    avatar: string;
    wallet: number;
    address_1: string;
    address_2: string;
    address_3: string;
    created_at: string;
    updated_at: string;
    user_id: number;
    address_1_lat: string;
    address_1_lng: string;
    address_2_lat: string;
    address_2_lng: string;
    address_3_lat: string;
    address_3_lng: string;
    dob: string;
    address_1_title: string;
    address_2_title: string;
    address_3_title: string;
}