import { UserProfile } from './index';
export interface User {
    id: number;
    name: string;
    phone: string;
    user_type: number;
    fb_id: string;
    token_android: string;
    token_ios: string;
    created_at: string;
    status: number;
    remember_token: string;
    online_status: number;
    last_login: string;
    lat: string;
    lng: string;
    delivery_status: number;
    device_token: string;
    in_queue: number;
    verify_token: string;
    verify_token_exp: string;
    deleted_at: string;
    is_email_verified: number;
    user_profile: UserProfile;
}