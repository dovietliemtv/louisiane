export interface HomePageModel {
    id: number,
    appinfo_description: string,
    appinfo_line: string,
    appinfo_store:string,
    appinfo_title: string,
    banner_description: string,
    banner_path: string,
    banner_title: string,
    created_at: string,
    updated_at: string,
}