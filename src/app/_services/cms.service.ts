import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Globals } from './../globals';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CmsService {

  constructor(
    private http: HttpClient,
    private global: Globals,
  ) { }

  getHompage(): Observable<any>{
    return this.http.get<any>(this.global.cmsUrl + `/api/homepage`);
  }
  getHowToOrderPage(): Observable<any>{
    return this.http.get<any>(this.global.cmsUrl + `/api/howtoorderpage`);
  }
  getContactUsPage(): Observable<any>{
    return this.http.get<any>(this.global.cmsUrl + `/api/contactuspage`);
  }
  getTermOfUsePage(): Observable<any>{
    return this.http.get<any>(this.global.cmsUrl + `/api/termofusepage`);
  }
  getPricacyPolitcyPage(): Observable<any>{
    return this.http.get<any>(this.global.cmsUrl + `/api/privacypolicypage`);
  }
  getHelpPage(): Observable<any>{
    return this.http.get<any>(this.global.cmsUrl + `/api/helppage`);
  }

  getDishsPage(): Observable<any>{
    return this.http.get<any>(this.global.cmsUrl + `/api/dishspage`);
  }

  getPaymentPage(): Observable<any>{
    return this.http.get<any>(this.global.cmsUrl + `/api/paymentpage`);
  }

  getGeneralOptions(): Observable<any>{
    return this.http.get<any>(this.global.cmsUrl + `/api/generaloptions`);
  }

  getAllPost(): Observable<any>{
    return this.http.get<any>(this.global.cmsUrl + `/api/posts`);
  }

  getNewsPage(): Observable<any>{
    return this.http.get<any>(this.global.cmsUrl + `/api/newspage`);
  }
  getNewsDetailPage(slug: string): Observable<any>{
    return this.http.get<any>(this.global.cmsUrl + `/api/posts/${slug}`);
  }

  getNewsSidebar(slug: string): Observable<any>{
    return this.http.get<any[]>(this.global.cmsUrl + `/api/postssidebar/${slug}`);
  }

  
  getMediaUrl () {
    return this.global.cmsUrl + '/storage/app/media';
  }

}
