import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class IndexedDBService {

  private DB_NAME = 'AddToCart'
  private DB_STORE_NAME = 'CartDetail';
  private DB_VERSION = 1;
  private request: any;
  constructor() {
   }

  initObjectStore() {
     this.request = window.indexedDB.open('AddToCart', this.DB_VERSION);
     this.request.onupgradeneeded = function(event) {
     let db = this.result;
     let store = db.createObjectStore(
       'CartDetail' , 
       { keyPath: 'id', autoIncrement: true });
       store.createIndex('dishID', 'dishID', { unique: false });
       store.createIndex('dishName', 'dishName', { unique: false });
       store.createIndex('quantity', 'quantity', { unique: false });
       store.createIndex('pricePerDish', 'pricePerDish', { unique: false });
       store.createIndex('price', 'price', { unique: false });
       store.createIndex('addons', 'addons', { unique: false });
   }
  }


  addNewRow(orderData) {
    let request = window.indexedDB.open('AddToCart', this.DB_VERSION);
    request.onsuccess = function(e) {
      let db = request.result;
      var transaction = db.transaction(["CartDetail"], "readwrite");
      // report on the success of the transaction completing, when everything is done
      transaction.oncomplete = function(event) {
        console.log('Transaction completed.') ;
      };
      transaction.onerror = function(event) {
        console.log('Transaction not opened due to error. Duplicate items not allowed.') ;
      };

      var objectStore = transaction.objectStore("CartDetail");
      var objectStoreRequest = objectStore.add(orderData);
      objectStoreRequest.onsuccess = function(event) {
        console.log('added!') ;
      };
    }
  }


  readData() {
    let request = window.indexedDB.open('AddToCart', this.DB_VERSION);
    const promise = new Promise<any[]>((resolve, reject) => {
      request.onsuccess = function(e) {
        let db = request.result;
        var objectStore =  db.transaction(["CartDetail"], "readwrite").objectStore("CartDetail");
        let req = objectStore.getAll();
        req.onsuccess = function(event) {
            let data = req.result;
            resolve(data);
        };
      }
    });
    return promise;
  }

  delectRowByDishID(id: number) {
    let request = window.indexedDB.open('AddToCart', this.DB_VERSION);
    const promise = new Promise((resolve, reject) => {
      request.onsuccess = function(e) {
        let db = request.result;
        var keyRangeValue = IDBKeyRange.only(id);
        let objectStore =  db.transaction(["CartDetail"], "readwrite").objectStore("CartDetail");
        // let req = objectStore.get(id);
        objectStore.openCursor(keyRangeValue).onsuccess = function(event) {
          let cursor = this.result;
          if(cursor) {
            if(cursor.value.id === id) {
              let req = cursor.delete();
              req.onsuccess = function() {
                  console.log('Deleted');
                  resolve(200);
              };
              req.onerror = function() {
                console.log('Cannot deleted');
                resolve(404);
              };
            }
          }
        }
      }
    });
    return promise;
  }

  updateRowDish(id: number, quantity: number, price: number) {
    let request = window.indexedDB.open('AddToCart', this.DB_VERSION);
    const promise = new Promise((resolve, reject) => {
      request.onsuccess = function(e) {
        let db = request.result;
        var keyRangeValue = IDBKeyRange.only(id);
        let objectStore =  db.transaction(["CartDetail"], "readwrite").objectStore("CartDetail");
        // let req = objectStore.get(id);
        objectStore.openCursor(keyRangeValue).onsuccess = function(event) {
          let cursor = this.result;
          if(cursor) {
            if(cursor.value.id === id) {
              const updateData = cursor.value;
              updateData.quantity = quantity;
              updateData.price = price;
              let req = cursor.update(updateData);
              req.onsuccess = function() {
                  console.log('Updated');
                  resolve(200);
              };
              req.onerror = function() {
                  console.log('Cannot Updated');
                  resolve(404);
              };
            }
          }
        }
      }
    });
    return promise;
  }
  clearData() {
    let request = window.indexedDB.open('AddToCart', this.DB_VERSION);
    const promise = new Promise((resolve, reject) => {
      request.onsuccess = function(e) {
        let db = request.result;
        var objectStore =  db.transaction(["CartDetail"], "readwrite").objectStore("CartDetail");
        let req = objectStore.clear();
        req.onsuccess = function(event) {
            resolve(200);
        };
      }
    });
    return promise;
  }
   
}
