import { Injectable } from '@angular/core';
import { Observable, observable } from 'rxjs';

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { User } from '../_models/index';
import { Globals } from './../globals';
import { HelpService, DeviceIDService } from './help.service';
import { map } from 'rxjs/operators';
import { pipe } from '@angular/core/src/render3/pipe';
import { Store } from '@ngrx/store';

@Injectable()
export class AuthService {

  private phoneNumber: string;
  private token: string;

  constructor(
    private help: HelpService,
    private http: HttpClient,
    private global: Globals,
    private store: Store<any>
    ) {
  }

  getLoginState() {
    return this.store.select('loginReducer');
  }
  updateState(obj) {
    this.store.dispatch({
      type: obj.action,
    });
  }


  getPhoneNumber() {
    return this.phoneNumber;
  }
  isAuth(): Boolean {
    let isAuth = false;
    if (localStorage.getItem('currentUser')) {
      isAuth = true;
    }
    return isAuth;
  }

  sendSMS(phoneNumber: string): Observable<any> {
    const deviceID = new DeviceIDService();
    if (phoneNumber) {
      this.phoneNumber = phoneNumber;
      const httpOptions = {
        headers: new HttpHeaders({
          'Content-Type':  'application/json',
          'device-id': deviceID.getDeviceID(),
          'device-type': 'ios',
          'X-Host-Override': this.global.host
        })
      };
      return this.http.get<any>(this.global.baseUrl +  `/auth/sms_verify?phone=${phoneNumber}&app_target=customer`, httpOptions)
      .pipe(map(
        response => {
          if (response) {
            localStorage.setItem('deviceID', deviceID.getDeviceID());
            return response;
          }
        }
      ));
    }
  }

  verifySMS(code: string): Observable<any> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'device-id': localStorage.getItem('deviceID'),
        'device-type': 'web',
        'X-Host-Override': this.global.host
      })
    };
    return this.http.get<any>
    (this.global.baseUrl +  `/auth/verify?
    phone=${this.getPhoneNumber()}&
    app_target=customer&verify_token=${code}&
    device_id=${localStorage.getItem('deviceID')}&
    device_type=web`, httpOptions)
    .pipe(map(
      user => {
        if (user && user.result.token) {
          return user;
        }
      }
    ));
  }

   logout(): Observable<any> {
    if (this.isAuth()) {
      const token = localStorage.getItem('token');
      const httpOptions = {
        headers: new HttpHeaders({
          'Content-Type':  'application/json',
          'device-id': localStorage.getItem('deviceID'),
          'token': token,
          'device-type': 'web',
          'X-Host-Override': this.global.host
        })
      };
      return this.http.get<any>(this.global.baseUrl +  `/auth/logout`, httpOptions);
      // .pipe(map(
      //   response => {
      //     if (response) {
      //       return response;
      //     }
      //   }
      // ));
    }
  }
}
