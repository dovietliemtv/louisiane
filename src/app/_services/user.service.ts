import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { User } from '../_models/index';
import { Globals } from './../globals';
import { Observable } from 'rxjs';
import {map} from 'rxjs/operators';
import { HelpService } from './help.service';
import {ACTION_LOGIN, ACTION_LOADING_SHOW, ACTION_LOGOUT, ACTION_LOADING_HIDE} from '../store/actions/index';

@Injectable({
  providedIn: 'root'
})
export class UserService {
    private currentUser: any;
    constructor(
        private http: HttpClient,
        private global: Globals,
        private help: HelpService
    ) { }



    getCurrentUser() {
      if (localStorage.getItem('currentUser')) {
         this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
      } else {
        this.currentUser = null;
      }
      return this.currentUser;
    }
    deleteCurrentUser() {
      localStorage.removeItem('currentUser');
    }
    updateCurrentUser(currentUser: any) {
      if (localStorage.getItem('currentUser')) {
        localStorage.removeItem('currentUser');
        localStorage.setItem('currentUser', JSON.stringify(currentUser))
      } else {
        return;
      }
    }

    editAvatar(file: any): Observable<any> {
      if(localStorage.getItem('currentUser')){
        const deviceID = localStorage.getItem('deviceID');
        const token = localStorage.getItem('token');
        const uploadData = new FormData();
        uploadData.append('avatar', file);
        const httpOptions = {
          headers: new HttpHeaders({
            'device-id': deviceID,
            'token': token,
            'device-type': 'web',
            'X-Host-Override': this.global.host,
          })
        };
        return this.http.post<any>(this.global.baseUrl + `/editavatar`, uploadData, httpOptions)
        .pipe(map(
          response => {
            return response
          }
        ));
      }else{
        console.log('Is not current user');
        return;
      }
    }

    editProfile(
      name,
      email,
      address_1,
      address_2,
      address_3,
      dob,
      location1,
      location2,
      location3
    ) {
      const deviceID = localStorage.getItem('deviceID');
      const token = localStorage.getItem('token');
      const httpOptions = {
        headers: new HttpHeaders({
          'device-id': deviceID,
          'token': token,
          'device-type': 'web',
          'X-Host-Override': this.global.host,
        })
      };
      return this.http.get<any>(
        this.global.baseUrl +  `/editprofile?name=${name}&email=${email}&address_1=${address_1}&address_1_lat=${location1 ? location1.lat : ''}&address_1_lng=${location1 ? location1.lng : ''}&address_2=${address_2}&address_2_lat=${location2 ? location2.lat : ''}&address_2_lng=${location2 ? location2.lng : ''}&address_3=${address_3}&address_3_lat=${location3 ? location3.lat: ''}&address_3_lng=${location3 ? location3.lng : ''}&dob=${dob}`
      , httpOptions)
      .pipe(map(
        response => {
          if (response) {
            return response;
          }
        }
      ));
    }

    getLocation(address: string): Observable<any> {
      const token = localStorage.getItem('token');
      const httpOptions = {
        headers: new HttpHeaders({
          'Content-Type':  'application/json',
          'device-id': localStorage.getItem('deviceID'),
          'token': token,
          'device-type': 'web',
          'X-Host-Override': this.global.host
        })
      };
      const api = encodeURI( this.global.baseUrl + `/utility/google-proxy?url=https://maps.googleapis.com/maps/api/geocode/json?address=${address}`);
      return this.http.get<any>(api, httpOptions)
       .pipe(map(
         response => {
          if(response.status === 'OK'){
            return response.results[0].geometry.location
          } else {
            return;
          }
         }
       ))
    }

    getTextFromLocation(lat: any, lng: any): Observable<any> {
      const token = localStorage.getItem('token');
      const httpOptions = {
        headers: new HttpHeaders({
          'Content-Type':  'application/json',
          'device-id': localStorage.getItem('deviceID'),
          'token': token,
          'device-type': 'web',
          'X-Host-Override': this.global.host
        })
      };
      const api = encodeURI( this.global.baseUrl + `/utility/google-proxy?url=https://maps.googleapis.com/maps/api/geocode/json?latlng=${lat ? lat: ''},${lng ? lng : ''}`);
      return this.http.get<any>(api, httpOptions)
       .pipe(map(
         response => {
          if(response.status === 'OK'){
            return response.results[0].formatted_address
          } else {
            return;
          }
         }
       ))
    }

    getMyprofile(): Observable<any> {
      const token = localStorage.getItem('token');
      const httpOptions = {
        headers: new HttpHeaders({
          'Content-Type':  'application/json',
          'device-id': localStorage.getItem('deviceID'),
          'token': token,
          'device-type': 'web',
          'X-Host-Override': this.global.host
        })
      };
      return this.http.get<any>(
        this.global.baseUrl +  `/myprofile`
      , httpOptions)
      .pipe(map(
        response => {
          if (response) {
            return response;
          }
        },
        error => {
          return;
        }
      ));


    }

    // register(user: User) {
    //     return this.http.post(`/users/register`, user);
    // }

    // update(user: User) {
    //     return this.http.put(`/users/` + user.id, user);
    // }

    // delete(id: number) {
    //     return this.http.delete(`/users/` + id);
    // }
}
