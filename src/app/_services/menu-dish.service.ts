import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Globals } from './../globals';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { HelpService } from './help.service';
import {ACTION_LOGIN, ACTION_LOADING_SHOW, ACTION_LOGOUT, ACTION_LOADING_HIDE} from '../store/actions';

@Injectable()
export class MenuDishService {

  constructor(
    private http: HttpClient,
    private global: Globals,
    private help: HelpService
  ) { }

  getListMenuByID(id: number = this.global.partnerDefaultID): Observable<any> {
    const httpOptions = {
      headers: new HttpHeaders({
        'X-Host-Override': this.global.host
      })
    };
    return this.http.get<any>(this.global.baseUrl + `/restaurantinfo/${id}`, httpOptions);
  }

  getListDishBySearchText(searchtext: string): Observable<any> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'X-Host-Override': this.global.host
      })
    };
    const body = {
      search: searchtext,
      take: 100,
      page: 1,
    }
    return this.http.post<any>(this.global.baseUrl + `/search-dish`, body , httpOptions);
  }
  

  getListDishByMenuID(id: number): Observable<any> {
    const httpOptions = {
      headers: new HttpHeaders({
        'X-Host-Override': this.global.host
      })
    };
    return this.http.get<any>(this.global.baseUrl + `/dishes/${id}`, httpOptions);
  }
}
