import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Globals } from '../globals';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Store } from '@ngrx/store';
import {map} from 'rxjs/operators';
import { pipe} from 'rxjs';

import { IndexedDBService } from './indexed-db.service';
@Injectable({
  providedIn: 'root'
})
export class OrderService {

  private totalPriceDish: number;
  private totalPriceCart: number;
  private deleveryFree = 0;
  private distance = 0;

  
  private totalPriceAddons: number;
  // private deliveryFee = 5;
  constructor(
    private global: Globals,
    private http: HttpClient,
    private store: Store<any>,
    private indexedDBService: IndexedDBService
  ) { }

  getOrderHistory(): Observable<any> {
    const deviceID = localStorage.getItem('deviceID');
    const token = localStorage.getItem('token');
    const httpOptions = {
        headers: new HttpHeaders({
          'Content-Type':  'application/json',
          'device-id': deviceID,
          'token': token,
          'device-type': 'web',
          'X-Host-Override': this.global.host
        })
    };
    return this.http.get<any>(this.global.baseUrl +  `/myorders`, httpOptions);
  }

  getAddons(id: number) {
    // const addons = [
    //   {
    //     id: 1,
    //     name: 'Chọn size',
    //     slug:'chon-size',
    //     required: true,
    //     addon_info: [
    //       {
    //         id: 1,
    //         name: 'Size M',
    //         price_addon: 0
    //       },
    //       {
    //         id: 2,
    //         name: 'Size L',
    //         price_addon: 3
    //       },
    //       {
    //         id: 3,
    //         name: 'Size XL',
    //         price_addon: 2
    //       },
    //     ]
    //   },
    //   {
    //     id: 2,
    //     name: 'Thêm topping',
    //     slug:'them-topping',
    //     required: false,
    //     addon_info: [
    //       {
    //         id: 1,
    //         name: 'Tran chau den',
    //         price_addon: 1
    //       },
    //       {
    //         id: 2,
    //         name: 'Tran chau trang',
    //         price_addon: 2
    //       },
    //       {
    //         id: 3,
    //         name: 'Tran chau do',
    //         price_addon: 3
    //       },
    //     ]
    //   },
    //   {
    //     id: 3,
    //     name: 'Chọn muc da',
    //     slug:'chon-muc-da',
    //     required: true,
    //     addon_info: [
    //       {
    //         id: 1,
    //         name: '50%',
    //         price_addon: 0
    //       },
    //       {
    //         id: 2,
    //         name: '100%',
    //         price_addon: 1
    //       },
    //     ]
    //   }
    // ]

    const addons = null;
    return addons;
  }

  getBeginAddonPrice(addons: any) {
    let price = 0;
    addons.map( itemAddon => {
      if(itemAddon.required) {
        price += itemAddon.addon_info[0].price_addon
      }
    })
    return price;
  }

  getPriceDish(dish: any) {
    return dish.price;
  }
  
  getTotalPriceCart() {
    return this.totalPriceCart;
  }
  setTotalPriceCart(totalPriceCart) {
    this.totalPriceCart = totalPriceCart;
  }

  getTotalPriceDish() {
    return this.totalPriceDish;
  }
  setTotalPriceDish(totalPriceDish) {
    this.totalPriceDish = totalPriceDish;
  }
  getTotalPriceAddons() {
    return this.totalPriceAddons;
  }
  setTotalPriceAddons(totalPriceAddons) {
    this.totalPriceAddons = totalPriceAddons;
  }  

  getUpdateYourOrderState() {
    return this.store.select('updateYourOrderReducer');
  }
  updateYourOrderState(obj) {
    this.store.dispatch({
      type: obj.action,
      payload: obj.payload
    });
  }


  getPayState() {
    return this.store.select('paymentReducer');
  }
  updatePaymentState(obj) {
    this.store.dispatch({
      type: obj.action,
      payload: obj.payload
    });
  }

  getDeliveryFee(lat, lng): Observable<any> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'X-Host-Override': this.global.host
      })
    };
    let body = {
      lat: lat,
	    lng: lng,
	    list: [this.global.partnerDefaultID]
    }
    return this.http.post<any>
    (this.global.baseUrl +  `/deliveryfee`,body , httpOptions)
    .pipe(map(
      response => {
        if (response) {
          return response;
        }
      }
    ));
  }
  getDeleveryFee() {
    return this.deleveryFree;
  }
  setDeleveryFee(deleveryFree) {
     this.deleveryFree = deleveryFree;
  }
  getDistance() {
    return this.distance;
  }
  setDistance(distance) {
     this.distance = distance;
  }

  checkout(cartInfo): Observable<any> {
    const deviceID = localStorage.getItem('deviceID');
    const token = localStorage.getItem('token');
    const httpOptions = {
        headers: new HttpHeaders({
          'Content-Type':  'application/json',
          'device-id': deviceID,
          'token': token,
          'device-type': 'web',
          'X-Host-Override': this.global.host
        })
    };
    return this.http.post<any>(this.global.baseUrl +  `/checkout`, cartInfo , httpOptions);
  }

}
