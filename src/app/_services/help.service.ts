import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';

@Injectable()
export class HelpService {

  // private deviceID: string;
  constructor(
    private store: Store<any>
  ) { }

  getLoadingState() {
    return this.store.select('appLoadingReducer');
  }
  updateLoadingState(obj) {
    this.store.dispatch({
      type: obj.action,
    });
  }
}
export class DeviceIDService {
  private deviceID: string;
  constructor() {
    this.setDeviceID(this.createDeviceID());
  }
  createDeviceID() {
    let deviceID = '';
    const possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    const date = new Date(); // today's date and time in ISO format
    const myDate = Date.parse(date.toString());
    for (let i = 0; i < 5; i++) {
      deviceID += possible.charAt(Math.floor(Math.random() * possible.length));
    }
    deviceID += myDate;
    return deviceID;
  }
  getDeviceID() {
    return this.deviceID;
  }
  setDeviceID(deviceID) {
    this.deviceID = deviceID;
  }
}
