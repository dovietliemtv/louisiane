import { Component, Input, OnInit  } from '@angular/core';
import { DeviceDetectorService } from 'ngx-device-detector';
import { Store } from '@ngrx/store';
import { AuthService, HelpService, CmsService } from './_services';
import { ACTION_LOADING_SHOW, ACTION_LOADING_HIDE }  from './store/actions/index';
import { first } from 'rxjs/operators';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit   {
  title = 'louisiane';
  deviceInfo = null;
  loading = false;
  generalOptionCMS: any;
  constructor(
    private deviceService: DeviceDetectorService,
    private authService: AuthService,
    private help: HelpService,
    private store: Store<any>,
    private cmsService: CmsService
  ) {
    this.deviceInfo = deviceService.getDeviceInfo();
  }
  ngOnInit() {
    this.loading = true;
    this.help.getLoadingState()
    .subscribe(
      state => {
        this.loading = state.loading;
      }
    );

    this.cmsService.getGeneralOptions().pipe(first()).subscribe(
      response => {
        if (response) {
          if(response.status_code === 200) {
            this.generalOptionCMS = response.result;
          } else {
            alert(response.message)
          }
        } else {
          console.log('No response');
        }
      },
      error => {
        console.log(error)
      }
    );

  }

  get isMobile() {
    return this.deviceService.isMobile();
  }

  get isTablet() {
    return this.deviceService.isTablet();
  }

  get isDesktop() {
    return this.deviceService.isDesktop();
  }

}
