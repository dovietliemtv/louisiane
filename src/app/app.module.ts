import { BrowserModule } from '@angular/platform-browser';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA  } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { ReactiveFormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { DeviceDetectorService } from 'ngx-device-detector';
import { Title } from '@angular/platform-browser';
import { Globals } from './globals';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { StoreModule } from '@ngrx/store';
import {reducers } from './store/reducers/index';
import { DateValueAccessorModule } from 'angular-date-value-accessor';
import { ClickOutsideModule } from 'ng-click-outside';
import {FormsModule} from '@angular/forms';
import { SanitizeHtmlPipe } from './_pipe/index';



import {
  HeaderComponent,
  FooterComponent,
  ComingsoonComponent,
  UserInfoSideBarComponent,
  HeroSearchComponent,
  SearchComponent,
  MenuComponent,
  DishComponent,
  OrderInfoSidebarComponent,
  HeroSubPageComponent,
  OrderDetailChoseComponent,
  OptionsOfDishComponent,
  OptionsComponent,
  HeroStepPaymentComponent,
  PaymentComponent,
  StepsInPaymentComponent,
  Step1PaymentComponent,
  Step2PaymentComponent,
  Step3PaymentComponent
 } from './_components/index';
import {
  HomeComponent,
  ContactUsComponent,
  TermOfUseComponent,
  PrivacyPolicyComponent,
  ErrorPageComponent,
  LoginComponent,
  RegisterComponent,
  AccountSettingComponent,
  HelpComponent,
  OrdersHistoryComponent,
  DishsComponent,
  PaymentPageComponent,
  NewsComponent,
  NewsDetailComponent,
  HowToOrderComponent,
  SearchPageComponent
} from './_pages/index';
import {
  UserService,
  AuthService,
  HelpService,
  MenuDishService,
  OrderService,
  IndexedDBService
} from './_services/index';
import { NewsItemComponent } from './_components/news-item/news-item.component';
import { NewsSidebarComponent } from './_components/news-sidebar/news-sidebar.component';
@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    HomeComponent,
    ComingsoonComponent,
    PaymentComponent,
    ContactUsComponent,
    TermOfUseComponent,
    PrivacyPolicyComponent,
    ErrorPageComponent,
    LoginComponent,
    RegisterComponent,
    AccountSettingComponent,
    HelpComponent,
    OrdersHistoryComponent,
    UserInfoSideBarComponent,
    HeroSearchComponent,
    SearchComponent,
    MenuComponent,
    DishComponent,
    OrderInfoSidebarComponent,
    DishsComponent,
    HeroSubPageComponent,
    OrderDetailChoseComponent,
    OptionsOfDishComponent,
    OptionsComponent,
    HeroStepPaymentComponent,
    PaymentPageComponent,
    StepsInPaymentComponent,
    Step1PaymentComponent,
    Step2PaymentComponent,
    Step3PaymentComponent,
    HowToOrderComponent,
    NewsComponent,
    NewsDetailComponent,
    SearchComponent,
    SearchPageComponent,
    SanitizeHtmlPipe,
    NewsItemComponent,
    NewsSidebarComponent
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    HttpClientModule,
    AppRoutingModule,
    StoreModule.forRoot(reducers, {}),
    DateValueAccessorModule,
    ClickOutsideModule,
    FormsModule

  ],
  providers: [
    UserService,
    AuthService,
    DeviceDetectorService,
    Title,
    HelpService,
    Globals,
    MenuDishService,
    OrderService,
    IndexedDBService
  ],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ],
  exports: [ RouterModule ],
  bootstrap: [AppComponent]
})


export class AppModule {}
