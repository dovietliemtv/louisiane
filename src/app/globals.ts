
import { HttpHeaders } from '@angular/common/http';
export class Globals {
    baseUrl = 'http://olivery.localhost/api';
    cmsUrl = 'http://louisianecms.localhost';
    host = 'olivery.develop';
    partnerDefaultID = 13;
}
