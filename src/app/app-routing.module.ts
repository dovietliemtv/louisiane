import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import {
    HomeComponent ,
    ContactUsComponent,
    TermOfUseComponent,
    PrivacyPolicyComponent,
    ErrorPageComponent,
    LoginComponent,
    RegisterComponent,
    AccountSettingComponent,
    HelpComponent,
    OrdersHistoryComponent,
    DishsComponent,
    PaymentPageComponent,
    HowToOrderComponent,
    NewsComponent,
    NewsDetailComponent,
    SearchPageComponent
} from './_pages/index';

const appRoutes: Routes = [
    { path: '', component: HomeComponent },
    { path: 'home',   redirectTo: '/', pathMatch: 'full' },
    { path: 'contact-us', component: ContactUsComponent },
    { path: 'term-of-use', component: TermOfUseComponent },
    { path: 'privacy-policy', component: PrivacyPolicyComponent },
    { path: '404-page', component: ErrorPageComponent },
    { path: 'sign-in', component: LoginComponent },
    { path: 'register', component: RegisterComponent },
    { path: 'account-setting', component: AccountSettingComponent },
    { path: 'help', component: HelpComponent },
    { path: 'orders-history', component: OrdersHistoryComponent },
    { path: 'menu/:id', component: DishsComponent },
    { path: 'payment', component: PaymentPageComponent },
    { path: 'how-to-order', component: HowToOrderComponent },
    { path: 'news', component: NewsComponent },
    { path: 'news/:slug', component: NewsDetailComponent },
    { path: 's/:text', component: SearchPageComponent },

    // { path: 'login', component: LoginComponent },
    // { path: 'register', component: RegisterComponent },
    { path: '**', redirectTo: '/404-page' }
];
@NgModule({
    imports: [ RouterModule.forRoot(appRoutes) ],
    exports: [ RouterModule ]
  })
  export class AppRoutingModule {}