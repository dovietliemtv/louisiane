import { Component, OnInit, Input } from '@angular/core';
import { CmsService } from 'src/app/_services';

@Component({
  selector: 'app-payment',
  templateUrl: './payment.component.html',
})
export class PaymentComponent implements OnInit {

  @Input() generalOptionCMS: any;
  mediaUrl: string;
  constructor(
    private cmsService: CmsService
  ) {
    this.mediaUrl = this.cmsService.getMediaUrl();
  }

  ngOnInit() {
  }

}
