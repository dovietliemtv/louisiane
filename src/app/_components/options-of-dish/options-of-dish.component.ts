import { Component, OnInit, Output, Input, EventEmitter } from '@angular/core';
import { OrderService, IndexedDBService } from 'src/app/_services';
import { ACTION_UPDATEYOURORDER } from 'src/app/store/actions';
@Component({
  selector: 'app-options-of-dish',
  templateUrl: './options-of-dish.component.html',
})


export class OptionsOfDishComponent implements OnInit {

  @Input() addon: any;
  @Input() dish: any;
  @Input() popupAddon: boolean;
  @Output() closeAddonPopup = new EventEmitter;
  @Input() totalPrice = 0;
  @Input() totalPriceAddons = 0;
  priceAddonsRadio = 0;
  totalPriceDish = 0;
  numberDish = 1;
  oldVal = 0;
  orderDish = {
    dishID: null,
    dishName: null,
    quantity: null,
    pricePerDish: null,
    price: null,
    addons: null
  }
  constructor(
    private orderService: OrderService,
    private indexedDBService: IndexedDBService
  ) { 
  }

  ngOnInit() {

  }
  reInit() {
    this.numberDish = 1;
    this.totalPriceAddons = 0;
    this.orderService.setTotalPriceAddons(this.totalPriceAddons)
    this.priceAddonsRadio = 0;
    this.popupAddon = true;  
    if(this.dish) {
      this.totalPrice = this.orderService.getPriceDish(this.dish);
      this.totalPrice = this.roundNumber(this.totalPrice);

    }

  }
  roundNumber(num) {
    return Math.round(num * 100) / 100
  }
  increase() {
    this.numberDish++;
    if(this.addon && this.dish) {
      this.orderService.setTotalPriceAddons(this.totalPriceAddons);
      this.totalPrice = this.numberDish * (this.orderService.getPriceDish(this.dish) + this.orderService.getTotalPriceAddons() + this.priceAddonsRadio);
      this.totalPrice = this.roundNumber(this.totalPrice);

    }
  }
  deIncrease() {
    this.numberDish--;
    if(this.numberDish < 1) {
      this.numberDish = 1;
      return;
    }
    if(this.addon && this.dish) {
      this.orderService.setTotalPriceAddons(this.totalPriceAddons);
      this.totalPrice = this.numberDish * (this.orderService.getPriceDish(this.dish) + this.orderService.getTotalPriceAddons() + this.priceAddonsRadio);
      this.totalPrice = this.roundNumber(this.totalPrice);

    }
  }

  closePopupAddon(e) {
    if(this.popupAddon === true && e.target.id === 'ls-popup-addon'){
        this.popupAddon = false;        
        this.closeAddonPopup.emit(this.popupAddon)
    }
  }


  chooseAddonsFunc(e) {
    if(e.target.type === 'checkbox' && e.target.checked === true){
      this.totalPriceAddons += parseFloat(e.target.value);
      this.orderService.setTotalPriceAddons(this.totalPriceAddons);

      // reload total price
      this.totalPrice = this.numberDish * (this.orderService.getPriceDish(this.dish) + this.orderService.getTotalPriceAddons() + this.priceAddonsRadio);
      this.totalPrice = this.roundNumber(this.totalPrice);

    } else if(e.target.type === 'checkbox' && e.target.checked === false){
      this.totalPriceAddons -= parseFloat(e.target.value);
      this.orderService.setTotalPriceAddons(this.totalPriceAddons);

      // reload total price
      this.totalPrice = this.numberDish * (this.orderService.getPriceDish(this.dish) + this.orderService.getTotalPriceAddons() + this.priceAddonsRadio);
      this.totalPrice = this.roundNumber(this.totalPrice);

    } else if( e.target.type === 'radio' && e.target.checked === true ){

      this.priceAddonsRadio = 0;
      let name = e.target.name;
      let value = e.target.value;
      let radioInput = (<HTMLInputElement[]><any>document.querySelectorAll(`input[type=radio]`));
      radioInput.forEach(item => {
        if(item.checked) {
          this.priceAddonsRadio += parseFloat(item.value) ;
        }
      })
      this.totalPrice = this.numberDish * (this.orderService.getPriceDish(this.dish) + this.orderService.getTotalPriceAddons() + this.priceAddonsRadio);
      this.totalPrice = this.roundNumber(this.totalPrice);

    }
    
  }

  addtoCartFunc(e) {
    e.preventDefault();
      let addons = [];
      let addonInput = (<HTMLInputElement[]><any>document.querySelectorAll('.addonInput'));
      addonInput.forEach(item => {
        if(item.checked) {
          let obj = {
            key: item.dataset.addonsname,
            value: item.value
          };
          addons.push(obj);
        }
      })

      this.orderDish.dishID = this.dish.id;
      this.orderDish.dishName = this.dish.name;
      this.orderDish.quantity = this.numberDish;
      this.orderDish.pricePerDish = this.dish.price;
      this.orderDish.price = this.totalPrice;
      this.orderDish.addons = addons;


      if(this.orderDish) {
        this.indexedDBService.initObjectStore();
        this.indexedDBService.addNewRow(this.orderDish); 
        let totalPriceCart = 0;
        this.indexedDBService.readData().then( (result) => {
          result.map(item => {
            totalPriceCart += item.price;
          });
          totalPriceCart = this.roundNumber(totalPriceCart);
          this.orderService.setTotalPriceCart(totalPriceCart);
           
          this.orderService.updateYourOrderState({
            action: ACTION_UPDATEYOURORDER,
            payload: {
              deliveryFee: this.orderService.getDeleveryFee() ? this.orderService.getDeleveryFee() : 0,
              distance: this.orderService.getDistance() ? this.orderService.getDistance() : 0,  
              totalPriceCart: this.orderService.getTotalPriceCart()
            }
          });
          this.popupAddon = false;        

        }) 
      }
  }
}
