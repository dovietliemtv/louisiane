import { Component, OnInit, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import {
  AuthService, OrderService, HelpService, UserService, IndexedDBService
} from './../../_services/index';

import { ACTION_LOADING_SHOW, ACTION_PAYMENT_STEP_1, ACTION_LOADING_HIDE, ACTION_PAYMENT_STEP_2, ACTION_UPDATEYOURORDER} from '../../store/actions/index';

@Component({
  selector: 'app-step1-payment',
  templateUrl: './step1-payment.component.html',
})
export class Step1PaymentComponent implements OnInit {

  cartInfo: any;
  paymentFirstStepForm: FormGroup;
  firstStepSubmitted = false;
  locationError = false;
  location = {
    lat: 0,
    lng: 0,
  };
  deliveryFree: any;
  distance: any;
  dishs: any;
  loading = false;
  currentTime: any;
  deliveryTime: any;
  @Input() paymentPageCMS:any;
  constructor(
    private formBuilder: FormBuilder,
    private orderService: OrderService,
    private indexedDBService: IndexedDBService,
    private help: HelpService,
    private userSerive: UserService,

  ) {
    this.currentTime = this.getCurrentTime();
    this.deliveryTime = [
      {id: 1, name: 'As soon as possible', value: this.currentTime},
    ];
   }


  ngOnInit() {
    this.orderService.getPayState().subscribe(
      state => {
        if(state){
          this.cartInfo = state.cartInfo;   
          this.deliveryFree = state.cartInfo.delivery_fee || 0;
          this.distance = state.cartInfo.distance || 0;
          this.location.lat = state.cartInfo.lat || 0;
          this.location.lng = state.cartInfo.lng || 0;
        }else {
          console.log('state cannot init')
        }
      }
    )

    this.paymentFirstStepForm = this.formBuilder.group({
      fullname: new FormControl(
        this.cartInfo.delivery_name ? this.cartInfo.delivery_name : '',
        [ 
          Validators.required,
        ]
      ) ,
      phone: new FormControl(
        this.cartInfo.delivery_mobile ? this.cartInfo.delivery_mobile : '',
        [ 
          Validators.required,
        ]
      ),
      address: new FormControl(
        this.cartInfo.delivery_address ? this.cartInfo.delivery_address : '',
        [ 
          Validators.required,
        ]
      ),
      note: new FormControl(
        this.cartInfo.note ? this.cartInfo.note : '',
      ),
      putInFrontDoor: new FormControl(),
      deliveryTime: new FormControl(
        this.currentTime,
      ),
    });

  }


  get f() { return this.paymentFirstStepForm.controls; }

  onSearchChange(e) {
    if(e.target.value){
      this.help.updateLoadingState({
        action: ACTION_LOADING_SHOW
      });
      this.userSerive.getLocation(e.target.value).subscribe(
        location => {
          if(location){
            this.locationError = false;
            this.location = location;
            this.orderService.getDeliveryFee(this.location.lat, this.location.lng).subscribe(
              response => {
                if(response) {
                  if(response.status_code === 200) {
                    this.deliveryFree = response.result.price;
                    this.distance = response.result.distance;
                    this.orderService.setDeleveryFee(this.deliveryFree)
                    this.orderService.setDistance(this.distance)

                    // update deliveryFree
                    this.cartInfo.distance = this.distance;
                    this.cartInfo.delivery_fee = this.deliveryFree;

                    let totalPriceCart = 0;
                    this.indexedDBService.readData().then( (result) => {
                      result.forEach(item => {
                        totalPriceCart += item.price;
                      });
                      totalPriceCart = this.roundNumber(totalPriceCart);
                      this.orderService.setTotalPriceCart(totalPriceCart);
                      this.orderService.updateYourOrderState({
                        action: ACTION_UPDATEYOURORDER,
                        payload: {
                          deliveryFee: this.orderService.getDeleveryFee() ? this.orderService.getDeleveryFee() : 0,
                          distance: this.orderService.getDistance() ? this.orderService.getDeleveryFee() : 0,
                          totalPriceCart: totalPriceCart
                        }
                      });
                    })
                  } else {
                    alert('Cannot calculator Delivery Free! Maybe,it is very far!');
                    this.locationError = true;
                    console.log('Cannot calculator Delivery Free!')
                  }
                }
              },
              error => {
                console.log('Cannot call Api!')
              }
            )
            this.help.updateLoadingState({
              action: ACTION_LOADING_HIDE
            });
          }else{
            this.locationError = true;
            this.help.updateLoadingState({
              action: ACTION_LOADING_HIDE
            });
          }
        }
      )
    } else {
      this.locationError = false;
    }
  }


  firstStepOnSubmit = () => {
    this.firstStepSubmitted = true;
    this.loading = true;
    // stop here if form is invalid
    if (this.paymentFirstStepForm.invalid || this.locationError === true ) {
        return;
    }
    // this.cartInfo.total_price = 
    // this.cartInfo.user_name = 
    // this.cartInfo.user_address = 
    // this.cartInfo.user_address_lat = 
    // this.cartInfo.user_address_lng = 
    this.cartInfo.delivery_name = this.f.fullname.value;
    this.cartInfo.delivery_mobile = this.f.phone.value;
    this.cartInfo.delivery_address = this.f.address.value;
    this.cartInfo.delivery_time = this.f.deliveryTime.value || this.getCurrentTime();
    this.cartInfo.lat = this.location.lat;
    this.cartInfo.lng = this.location.lng;  
    this.cartInfo.contact_info = `${this.f.fullname.value} / ${this.f.phone.value}`;
    this.cartInfo.distance = this.distance;
    this.cartInfo.delivery_fee = this.deliveryFree;
    this.cartInfo.note = this.f.note.value;
    this.cartInfo.id_main_restaurant
    this.cartInfo.card_token_id
    this.cartInfo.putInFrontDoor = this.f.putInFrontDoor.value || false;
    this.goToSecondStep();
  }
  goToSecondStep(){
    this.indexedDBService.readData().then((result) => {
      this.dishs = result;
      if(result.length > 0) {
        this.orderService.updatePaymentState({
          action: ACTION_PAYMENT_STEP_2,
          payload: this.cartInfo
        })
      } else {
        alert('You should choose at least one dish!');
      }
    })
  }

  getCurrentTime = () =>{
    let now = new Date(); 
    let str = now.toISOString();
    let res = str.replace(/\.[0-9]{2,3}/, '');
    return res;
  }
  roundNumber(num) {
    return Math.round(num * 100) / 100
  }
}
