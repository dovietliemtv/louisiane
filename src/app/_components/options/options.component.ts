import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-options',
  templateUrl: './options.component.html',
})
export class OptionsComponent implements OnInit {

  @Input() addonItem: any;
  @Input() dish: any;
  @Output() chooseAddons = new EventEmitter;
  constructor() { }

  ngOnInit() {
  }
  selectAddons(e) {
    this.chooseAddons.emit(e);
  }



}
