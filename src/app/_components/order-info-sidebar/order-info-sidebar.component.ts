import { Component, OnInit } from '@angular/core';
import { OrderService, IndexedDBService, AuthService } from 'src/app/_services';
import { ACTION_UPDATEYOURORDER, ACTION_PAYMENT_STEP_1 } from 'src/app/store/actions';

@Component({
  selector: 'app-order-info-sidebar',
  templateUrl: './order-info-sidebar.component.html',
})
export class OrderInfoSidebarComponent implements OnInit {


  listDishs: any;
  totalPriceCart = 0;
  deliveryFee = 0;
  distance = 0;
  disableOrder = false;

  step1 = false;
  step2 = false;
  step3 = false;


  constructor(
    private orderService: OrderService,
    private authService: AuthService,
    private indexedDBService: IndexedDBService
  ) { 
 
  }
  ngOnInit() {
    // update dish
    if (this.authService.isAuth()) { 
      this.orderService.getUpdateYourOrderState().subscribe(
        state => {
          if(state) {
            if(state.updateyourorder) {
              this.deliveryFee = state.deliveryInfo.deliveryFee ? state.deliveryInfo.deliveryFee : 0;
              this.distance = state.deliveryInfo.distance ? state.deliveryInfo.distance : 0;
              this.totalPriceCart = state.deliveryInfo.totalPriceCart ? state.deliveryInfo.totalPriceCart + this.deliveryFee : 0;
              this.totalPriceCart = this.roundNumber(this.totalPriceCart);
              this.indexedDBService.readData().then((data) => {
                this.listDishs = data;
                if(this.listDishs.length === 0) {
                  this.disableOrder = true;
                } else {
                  this.disableOrder = false;

                }
              })
            }
          }
        }
      );
    } else {
      this.disableOrder = true;
    }

    this.orderService.getPayState().subscribe(
      state => {
        if(state){
            this.step1 = state.step1 ? true :  false;
            this.step2 = state.step2 ? true :  false;
            this.step3 = state.step3 ? true :  false;  
        }else {
          console.log('state cannot init')
        }
      }
    )


  }

  ngAfterContentInit() {
    if (this.authService.isAuth()) {
      let totalPriceCart = 0;
      this.indexedDBService.readData().then( (result) => {
        result.forEach(item => {
          totalPriceCart += item.price;
        });
        totalPriceCart = this.roundNumber(totalPriceCart);
        this.orderService.setTotalPriceCart(totalPriceCart);
        this.orderService.updateYourOrderState({
          action: ACTION_UPDATEYOURORDER,
          payload: {
            deliveryFee: this.orderService.getDeleveryFee() ? this.orderService.getDeleveryFee() : 0,
            distance: this.orderService.getDistance() ? this.orderService.getDistance() : 0,
            totalPriceCart: this.orderService.getTotalPriceCart()
          }
        });
      })
    }
  }


  calculatorTotalPriceCart() {
      this.totalPriceCart = 0;
      this.indexedDBService.readData().then((data) => {
      this.listDishs = data;
      this.listDishs.map( dish => {
        this.totalPriceCart += parseFloat(dish.price);
      });
      this.totalPriceCart += this.deliveryFee;
      this.totalPriceCart = this.roundNumber(this.totalPriceCart);
      this.orderService.setTotalPriceCart(this.totalPriceCart);
    })
  }

  roundNumber(num) {
    return Math.round(num * 100) / 100
  }

}
