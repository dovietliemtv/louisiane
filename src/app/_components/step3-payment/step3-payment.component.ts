import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { OrderService, IndexedDBService } from 'src/app/_services';
import { ACTION_PAYMENT_STEP_1 } from 'src/app/store/actions';

@Component({
  selector: 'app-step3-payment',
  templateUrl: './step3-payment.component.html',
})
export class Step3PaymentComponent implements OnInit, OnDestroy {

  cartInfo: any;
  @Input() paymentPageCMS:any;

  constructor(
    private orderService: OrderService,
    private indexedDBService: IndexedDBService
  ) { }

  ngOnInit() {

    this.orderService.getPayState().subscribe(
      state => {
        if(state){
          this.cartInfo = state.cartInfo;   
        }else {
          console.log('state cannot init')
        }
      }
    )
  }

  ngOnDestroy() {
    this.indexedDBService.clearData().then((result) => {
      if(result === 200) {
        console.log('Data was clear!');
      }
    })
  }

}
