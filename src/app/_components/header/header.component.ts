import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { User } from 'src/app/_models';
import { AuthService, UserService, HelpService } from 'src/app/_services/index';
import { first, filter } from 'rxjs/operators';
import {ACTION_LOGIN, ACTION_LOADING_SHOW, ACTION_LOGOUT, ACTION_LOADING_HIDE} from '../../store/actions/index';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
})
export class HeaderComponent implements OnInit {
  shown = false;
  currentUser: any;
  @Input() generalOptionCMS: any;

  returnUrl: string;
  loading = false;
    constructor(
      private router: Router,
      private route: ActivatedRoute,
      private authService: AuthService,
      private userSerive: UserService,
      private help: HelpService
  ) {

    if (this.authService.isAuth()) {
      this.userSerive.getMyprofile().subscribe(
        response => {
          if(response.status_code === 200) {
            this.currentUser = response.result;
            this.userSerive.updateCurrentUser(this.currentUser);
            this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
            this.authService.updateState({
              action: ACTION_LOGIN
            });

          } else {
            console.log(response)
          }
        },
        error => {
          console.log(error)
        }
      )  
    } else {
      this.authService.updateState({
        action: ACTION_LOGOUT
      });
    }

    this.router.events.pipe(
      filter(event => event instanceof NavigationEnd)
    ).subscribe(() => {
      this.shown = false;
    });

    // if (this.authService.isAuth()) {
    //   this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    //   this.authService.updateState({
    //     action: ACTION_LOGIN
    //   });
    // } else {
    //   this.authService.updateState({
    //     action: ACTION_LOGOUT
    //   });
    // }
  }
  ngOnInit() {
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
    this.authService.getLoginState().subscribe(
      state => {
        this.currentUser = state.login ? JSON.parse(localStorage.getItem('currentUser')) : null;
      }
    );
  }

  showMenu = (e) => {
    if (this.shown) {
      this.shown = false;
    } else {
      this.shown = true;
    }
  }
  logoutFunc(e) {
    e.preventDefault();
    // this.loading = true;
    this.help.updateLoadingState({
      action: ACTION_LOADING_SHOW
    });
    this.authService.logout()
    .pipe(first()).subscribe(
      response => {
        if (response.status_code === 200) {
          console.log('Logged Out');
          localStorage.removeItem('deviceID');
          localStorage.removeItem('token');
          localStorage.removeItem('currentUser');
          // this.loading = false;
          this.help.updateLoadingState({
            action: ACTION_LOADING_HIDE
          });
          this.authService.updateState({
            action: ACTION_LOGOUT
          });
        } else {
          console.log('BUG! Cannot Logout');
          this.help.updateLoadingState({
            action: ACTION_LOADING_HIDE
          });
        }
      }
    );
    this.router.navigate([this.returnUrl]);
  }


}
