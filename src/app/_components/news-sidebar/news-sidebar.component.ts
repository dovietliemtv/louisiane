import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-news-sidebar',
  templateUrl: './news-sidebar.component.html',
})
export class NewsSidebarComponent implements OnInit {

  @Input() newsSidebarCMS: any;
  constructor() { }

  ngOnInit() {
  }

}
