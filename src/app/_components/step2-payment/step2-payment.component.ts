import { 
  Component, 
  OnInit, 
  Input, 
  ViewChild, 
  ElementRef, 
  ChangeDetectorRef,
  AfterViewInit,
  OnDestroy
 } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import {
  AuthService, OrderService, HelpService, UserService, IndexedDBService
} from './../../_services/index';
import { ACTION_LOADING_SHOW, ACTION_PAYMENT_STEP_1, ACTION_LOADING_HIDE, ACTION_PAYMENT_STEP_3, ACTION_UPDATEYOURORDER} from '../../store/actions/index';
import { NgForm } from '@angular/forms';
import { Globals } from 'src/app/globals';
declare var stripe: any;
declare var elements: any;
declare var card: any;


@Component({
  selector: 'app-step2-payment',
  templateUrl: './step2-payment.component.html',
})
export class Step2PaymentComponent implements OnInit, OnDestroy {

  cartInfo: any;
  paymentSecondStepForm: FormGroup;
  secondStepSubmitted = false;
  balance: number;
  balanceAfterPayment: number;
  needToPay: number;
  creditPayment = false;

  @ViewChild('cardInfoStripe') cardInfoStripe: ElementRef;
  card: any;
  // cardHandler: any;
  cardHandler = this.onChange.bind(this);

  error: string;
  constructor(
    private formBuilder: FormBuilder,
    private orderService: OrderService,
    private indexedDBService: IndexedDBService,
    private help: HelpService,
    private globals: Globals,
    private userSerive: UserService,
    private cd: ChangeDetectorRef

  ) {

    // this.card = elements.create('card');
  }

  ngOnInit() {
    this.orderService.getPayState().subscribe(
      state => {
        if(state){
          this.cartInfo = state.cartInfo; 
          this.userSerive.getMyprofile().subscribe(
            response => {
              if(response) {
                if(response.status_code === 200) {
                    this.balance = response.result.user_profile.wallet;
                    this.balance = this.roundNumber(this.balance);
                    this.balanceAfterPayment =  this.calculatorBalanceAfterPayment(this.balance, (this.orderService.getTotalPriceCart() +  this.orderService.getDeleveryFee()));
                    this.balanceAfterPayment = this.roundNumber(this.balanceAfterPayment);
                    this.needToPay = -(this.balance - (this.balanceAfterPayment + this.orderService.getTotalPriceCart() + this.orderService.getDeleveryFee() ));
                    this.needToPay = this.roundNumber(this.needToPay);
                    // update info for cart detail when submit
                    this.cartInfo.total_price = this.roundNumber(this.orderService.getTotalPriceCart() + this.orderService.getDeleveryFee()) ;
                    this.cartInfo.user_address = response.result.user_profile.address_1 || response.result.user_profile.address_2 || response.result.user_profile.address_3;
                    this.cartInfo.user_address_lat = response.result.user_profile.address_1_lat || response.result.user_profile.address_2_lat || response.result.user_profile.address_3_lat;
                    this.cartInfo.user_address_lng = response.result.user_profile.address_1_lng || response.result.user_profile.address_2_lng || response.result.user_profile.address_3_lng;
                    this.cartInfo.user_name = response.result.name || response.result.phone;
                    this.cartInfo.id_main_restaurant = this.globals.partnerDefaultID;
      
                    if (state.step2) {
                      if (this.needToPay > 0 ) {
                        this.creditPayment = true;
                        this.createCardCheckout();
                      } else {
                       
                        this.creditPayment = false;
                      }
                    }
                }
              }
            }
          )
        
        }else {
          console.log('state cannot init')
        }
      }
    )
    this.paymentSecondStepForm = this.formBuilder.group({});  
  }
  createCardCheckout() {
    card.mount(this.cardInfoStripe.nativeElement);
    card.addEventListener('change', this.cardHandler);
  }
  removeCardCheckout() {
    card.removeEventListener('change', this.cardHandler);
    card.destroy();
  }
  ngOnDestroy() {
    if(this.cardInfoStripe.nativeElement) {
      card.unmount(this.cardInfoStripe.nativeElement);
    }
  }
  onChange({ error }) {
    if (error) {
      this.error = error.message;
    } else {
      this.error = null;
    }
    this.cd.detectChanges();
  }

  async onSubmit(form: NgForm) {
    this.help.updateLoadingState({
      action: ACTION_LOADING_SHOW
    });
    if(this.creditPayment) {
      const { token, error } = await stripe.createToken(card);
      if (error) {
        console.log('Something is wrong:', error);
        this.help.updateLoadingState({
          action: ACTION_LOADING_HIDE
        });
      } else {
        // console.log('Success!', token);
        this.cartInfo.card_token_id = token.id;
        this.indexedDBService.readData().then((result) => {
          let dishs = [];
          result.forEach( item => {
            let dish = {
              id: Number,
              quantity: Number,
              price: Number,
              note: String,
              addons: Array
            };
            dish.id = item.dishID;
            dish.quantity = item.quantity;
            dish.price = item.pricePerDish;
            dish.note = item.note ? item.note : '';
            dish.addons = item.addons;
            dishs.push(dish);
          })
          this.cartInfo.dish = dishs;
          // console.log(this.cartInfo)
          this.orderService.checkout(this.cartInfo).subscribe(
            response => {
              if(response) {
                if(response.status_code === 200){
                    this.goToThirdStep();
                    this.help.updateLoadingState({
                      action: ACTION_LOADING_HIDE
                    });
                } else {
                  alert(response.result);
                  this.help.updateLoadingState({
                    action: ACTION_LOADING_HIDE
                  });
                }
              }
            }
          )

        })
        // ...send the token to the your backend to process the charge
      }
    } else {
      this.indexedDBService.readData().then( (result) => {
        let dishs = [];
        result.forEach( item => {
          let dish = {
            id: Number,
            quantity: Number,
            price: Number,
            note: String,
            addons: Array
          };
          dish.id = item.dishID;
          dish.quantity = item.quantity;
          dish.price = item.pricePerDish;
          dish.note = item.note ? item.note : '';
          dish.addons = item.addons;
          dishs.push(dish);
        })
        this.cartInfo.dish = dishs;
        // console.log(this.cartInfo)
        this.orderService.checkout(this.cartInfo).subscribe(
          response => {
            if(response) {
              if(response.status_code === 200){
                this.goToThirdStep();
                this.help.updateLoadingState({
                  action: ACTION_LOADING_HIDE
                });
              } else {
                alert(response.result);
                this.help.updateLoadingState({
                  action: ACTION_LOADING_HIDE
                });
              }
            }
          }
        )
      })
    }
  }

  get s() { return this.paymentSecondStepForm.controls; }

  goToFirstStep(){
    this.orderService.updatePaymentState({
      action: ACTION_PAYMENT_STEP_1,
      payload: this.cartInfo
    })
  }
  goToThirdStep() {
    this.orderService.updatePaymentState({
      action: ACTION_PAYMENT_STEP_3,
      payload: this.cartInfo
    })
  }
  roundNumber(num) {
    return Math.round(num * 100) / 100
  }
  calculatorBalanceAfterPayment(owallet: number, totalPriceCart: number) {
    if(owallet === 0){
      return 0;
    } else if(owallet >= totalPriceCart){
      return owallet - totalPriceCart;
    } else {
      if ((totalPriceCart - owallet) <= 0.5) {
        return (owallet + 0.5) - totalPriceCart;
      } else {
        return 0;
      }
    }
  }


}
