import { Component, OnInit, Input } from '@angular/core';
import { CmsService } from 'src/app/_services';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
})
export class FooterComponent implements OnInit {

  @Input() generalOptionCMS: any;
  mediaUrl: string;
  constructor(
    private cmsService: CmsService
  ) {
    this.mediaUrl = this.cmsService.getMediaUrl();
  }

  ngOnInit() {
  }

}
