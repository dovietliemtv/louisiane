import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
@Component({
  selector: 'app-dish',
  templateUrl: './dish.component.html',
})
export class DishComponent implements OnInit {

  @Input() dish: any;
  @Output() open = new EventEmitter();
  @Output() addToCart = new EventEmitter();

  constructor(
  ) { }

  ngOnInit() {
  }
  imgClick(dish) {
    this.open.emit(dish);
  }
  addToCartFunc(dish){
    this.addToCart.emit(dish);
    
  }

}
