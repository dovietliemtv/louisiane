import { Component, OnInit, Input } from '@angular/core';
import { CmsService } from 'src/app/_services';
import {first} from 'rxjs/operators';
import {
  HomePageModel
} from '../../_models';
@Component({
  selector: 'app-hero-search',
  templateUrl: './hero-search.component.html',

})
export class HeroSearchComponent implements OnInit {

  @Input() homepageCMS: HomePageModel;
  constructor(
    private cms: CmsService
  ) {       
  }

  ngOnInit() {
  }

}
