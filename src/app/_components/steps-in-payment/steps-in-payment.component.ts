import { Component, OnInit, OnDestroy, Input  } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { NgForm } from '@angular/forms';
import { ACTION_PAYMENT_STEP_0, ACTION_PAYMENT_STEP_1 } from 'src/app/store/actions';
import { 
  OrderService, 
  IndexedDBService,
  CmsService
 } from 'src/app/_services';
import { first } from 'rxjs/operators';

@Component({
  selector: 'app-steps-in-payment',
  templateUrl: './steps-in-payment.component.html',
})
export class StepsInPaymentComponent implements OnInit, OnDestroy {

  title = 'Louisiane - Payment';
  cartInfo: any;
  step1 = false;
  step2 = false;
  step3 = false;
  @Input() paymentPageCMS: any;
  constructor(
    private titleService: Title,
    private orderService: OrderService,
    private indexedDBService: IndexedDBService,
    private cmsService: CmsService
  ) {

    titleService.setTitle(this.title);

   }// end constructor

  ngOnInit() {
    this.orderService.getPayState().subscribe(
      state => {
        if(state){
          this.cartInfo = state.cartInfo;
          if(state.step1) {
            this.step1 = true;
            this.step2 = false;
            this.step3 = false;
          } else if (state.step2) {
            this.step2 = true;
            this.step1 = false;
            this.step3 = false;
          } else {
            this.step3 = true;
            this.step2 = false;
            this.step1 = false;
          }
        }else {
          console.log('state cannot init')
        }
      }
    )

  }
  ngOnDestroy() {
    this.orderService.updatePaymentState({
      action: ACTION_PAYMENT_STEP_1,
      payload: this.cartInfo
    })
}


  
}// end Class
