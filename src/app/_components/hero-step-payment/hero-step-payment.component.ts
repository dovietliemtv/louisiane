import { Component, OnInit, Input } from '@angular/core';
import { OrderService } from 'src/app/_services';

@Component({
  selector: 'app-hero-step-payment',
  templateUrl: './hero-step-payment.component.html',
})
export class HeroStepPaymentComponent implements OnInit {

  firstStep = false
  secondStep = false
  thirdStep = false
  @Input() paymentPageCMS: any;
  constructor(
    private orderService: OrderService
  ) { }

  ngOnInit() {
    this.orderService.getPayState().subscribe(
      state => {
        if(state){
          this.firstStep = state.step1;
          this.secondStep = state.step2;
          this.thirdStep = state.step3;
        }else {
          console.log('state cannot init')
        }
      }
    )

  }

}
