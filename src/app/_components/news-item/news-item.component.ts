import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-news-item',
  templateUrl: './news-item.component.html',
})
export class NewsItemComponent implements OnInit {

  @Input() news: any;
  constructor() { }

  ngOnInit() {
  }

}
