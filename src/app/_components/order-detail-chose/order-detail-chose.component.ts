import { Component, OnInit, Input } from '@angular/core';
import { IndexedDBService, OrderService } from 'src/app/_services';
import { ACTION_UPDATEYOURORDER } from 'src/app/store/actions';

@Component({
  selector: 'app-order-detail-chose',
  templateUrl: './order-detail-chose.component.html',
})
export class OrderDetailChoseComponent implements OnInit {

  @Input() dish: any;
  numberDish: number;
  dishPrice: number;
  clickedOnNumber= false;
  constructor(
    private indexedDBService: IndexedDBService,
    private orderService: OrderService
  ) { }

  ngOnInit() {
    this.numberDish = this.dish.quantity;
    this.dishPrice = this.dish.price;
    this.dishPrice = this.roundNumber(this.dishPrice);
  }
  deleteFunc(e, dish) {
    e.preventDefault();
    this.indexedDBService.delectRowByDishID(dish.id).then( (result) => {
      if(result === 200) {
        let totalPriceCart = 0;
        this.indexedDBService.readData().then( (result) => {
          result.forEach(item => {
            totalPriceCart += item.price;
          });
          totalPriceCart = this.roundNumber(totalPriceCart);
          this.orderService.setTotalPriceCart(totalPriceCart);
           
          this.orderService.updateYourOrderState({
            action: ACTION_UPDATEYOURORDER,
            payload: {
              deliveryFee: this.orderService.getDeleveryFee() ? this.orderService.getDeleveryFee() : 0,
              distance: this.orderService.getDistance() ? this.orderService.getDistance() : 0,
              totalPriceCart: this.orderService.getTotalPriceCart()
            }
          });
        })
        
        // this.orderService.updateYourOrderState({
        //   action: ACTION_UPDATEYOURORDER,
        //   payload: {
        //     deliveryFee: 0,
        //     distance: 0,
        //     totalPriceCart: this.orderService.getTotalPriceCart()
        //   }
        // });
      }
    })
  }

  increase(e, dish){
    e.preventDefault();
    this.numberDish++;
    // Re-calc price dish
    let addonTotalPrice = 0;
    if(dish.addons) {
      dish.addons.forEach(addon => {
        addonTotalPrice += parseFloat(addon.value);
      });
    }

    this.dishPrice = (dish.pricePerDish + addonTotalPrice) * this.numberDish;
    this.dishPrice = this.roundNumber(this.dishPrice);

    this.clickedOnNumber = true;
  }

  deIncrease(e, dish){
    e.preventDefault();
    this.numberDish--;
    if(this.numberDish < 1) this.numberDish = 1;

    // Re-calc price dish
    let addonTotalPrice = 0;
    if(dish.addons) {
      dish.addons.forEach(addon => {
        addonTotalPrice += parseFloat(addon.value);
      });
    }
    this.dishPrice = (dish.pricePerDish + addonTotalPrice) * this.numberDish;
    this.dishPrice = this.roundNumber(this.dishPrice);

    this.clickedOnNumber = true;
  }
  roundNumber(num) {
    return Math.round(num * 100) / 100
  }
  updateDishInfo(e, dish) {
    e.preventDefault();
    if(!this.clickedOnNumber) return;
    this.indexedDBService.updateRowDish(dish.id, this.numberDish, this.dishPrice).then((result) => {
      if(result === 200) {
        // this.orderService.reSetTotalPriceCart();
        let totalPriceCart = 0;
        this.indexedDBService.readData().then( (result) => {
          result.forEach(item => {
            totalPriceCart += item.price;
          });
          totalPriceCart = this.roundNumber(totalPriceCart);
          this.orderService.setTotalPriceCart(totalPriceCart);
          this.orderService.updateYourOrderState({
            action: ACTION_UPDATEYOURORDER,
            payload: {
              deliveryFee: this.orderService.getDeleveryFee(),
              distance: this.orderService.getDistance(),
              totalPriceCart: this.orderService.getTotalPriceCart()
            }
          });
        })

      }
    })
  }
}
