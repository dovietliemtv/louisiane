import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService, HelpService } from 'src/app/_services';
import {ACTION_LOGIN, ACTION_LOADING_SHOW, ACTION_LOGOUT, ACTION_LOADING_HIDE} from '../../store/actions/index';
import {first} from 'rxjs/operators';
@Component({
  selector: 'app-user-info-side-bar',
  templateUrl: './user-info-side-bar.component.html',
})
export class UserInfoSideBarComponent implements OnInit {

  currentUser: any;
  returnUrl: string;
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private authService: AuthService,
    private help: HelpService
  ) {

   
    if (this.authService.isAuth()) {
      this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
      this.authService.updateState({
        action: ACTION_LOGIN
      });
    } else {
      this.authService.updateState({
        action: ACTION_LOGOUT
      });
    }
   }

  ngOnInit() {
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
    this.authService.getLoginState().subscribe(
      state => {
        this.currentUser = state.login ? JSON.parse(localStorage.getItem('currentUser')) : null;
      }
    );
  }

  logoutFunc(e) {
    e.preventDefault();
    // this.loading = true;
    this.help.updateLoadingState({
      action: ACTION_LOADING_SHOW
    });
    this.authService.logout()
    .pipe(first()).subscribe(
      response => {
        if (response.status_code === 200) {
          console.log('Logged Out');
          localStorage.removeItem('deviceID');
          localStorage.removeItem('currentUser');
          // this.loading = false;
          this.help.updateLoadingState({
            action: ACTION_LOADING_HIDE
          });
          this.authService.updateState({
            action: ACTION_LOGOUT
          });
        } else {
          console.log('BUG! Cannot Logout');
          this.help.updateLoadingState({
            action: ACTION_LOADING_HIDE
          });
        }
      }
    );
    this.router.navigate([this.returnUrl]);
  }


}
