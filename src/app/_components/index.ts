export * from './header/header.component';
export * from './footer/footer.component';
export * from './comingsoon/comingsoon.component';
export * from './payment/payment.component';
export * from './user-info-side-bar/user-info-side-bar.component';

export * from './dish/dish.component';

export * from './hero-search/hero-search.component';
export * from './menu/menu.component';
export * from './order-info-sidebar/order-info-sidebar.component';
export * from './search/search.component';
export * from './hero-sub-page/hero-sub-page.component';
export * from './order-detail-chose/order-detail-chose.component';

export * from './options-of-dish/options-of-dish.component';
export * from './options/options.component';
export * from './hero-step-payment/hero-step-payment.component';
export * from './steps-in-payment/steps-in-payment.component';
export * from './step1-payment/step1-payment.component';
export * from './step2-payment/step2-payment.component';
export * from './step3-payment/step3-payment.component';
