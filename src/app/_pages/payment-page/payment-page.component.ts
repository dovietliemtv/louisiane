import { Component, OnInit } from '@angular/core';
import { IndexedDBService, OrderService, AuthService, CmsService } from 'src/app/_services';
import { ACTION_UPDATEYOURORDER, ACTION_PAYMENT_STEP_1 } from 'src/app/store/actions';
import { first } from 'rxjs/operators';

@Component({
  selector: 'app-payment-page',
  templateUrl: './payment-page.component.html',
})
export class PaymentPageComponent implements OnInit {

  existOrder: any;
  paymentPageCMS: any;
  constructor(
    private indexedDBService: IndexedDBService,
    private orderService: OrderService,
     private authService: AuthService,
     private cmsService: CmsService
  ) { }


  ngOnInit() {
    this.indexedDBService.readData().then((result) => {
      if(result){
        this.existOrder = result;
      }else{
        console.log('Read indexedDB is Error')
      }
    })

    this.cmsService.getPaymentPage().pipe(first()).subscribe(
      response => {
        if (response) {
          if(response.status_code === 200) {
            this.paymentPageCMS = response.result;
          } else {
            alert(response.message)
          }
        } else {
          console.log('No response');
        }
      },
      error => {
        console.log(error)
      }
    );
   
  }


  roundNumber(num) {
    return Math.round(num * 100) / 100
  }

}
