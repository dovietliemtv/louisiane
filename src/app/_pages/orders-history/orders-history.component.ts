import { Component, OnInit } from '@angular/core';
import { UserService, OrderService } from 'src/app/_services';
import { pipe } from '@angular/core/src/render3/pipe';

@Component({
  selector: 'app-orders-history',
  templateUrl: './orders-history.component.html',
})
export class OrdersHistoryComponent implements OnInit {

  currentUser: any;
  listHistoryOrder: any[];
  constructor(
    private userService: UserService,
    private orderService: OrderService,
  ) { }

  ngOnInit() {
    this.checkUserLoggedin();

    this.orderService.getOrderHistory().subscribe(
      response => {
        if (response.status_code === 200) {
          this.listHistoryOrder = response.result.data;
        } else {
          console.log(response);
        }
      },
      error => {
        console.log('Cannot send request');
        console.log(error);
      }
    );

  }

  checkUserLoggedin = () => {
    if (localStorage.getItem('currentUser')) {
      this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    } else {
      this.currentUser = null;
    }
  }
}
