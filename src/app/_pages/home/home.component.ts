import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { 
  MenuDishService, 
  HelpService, 
  CmsService 
} from '../../_services/index';
import {first, map} from 'rxjs/operators';
import {ACTION_LOGIN, ACTION_LOADING_SHOW, ACTION_LOGOUT, ACTION_LOADING_HIDE} from '../../store/actions/index';
import { pipe } from 'rxjs';
import { HeroSearchComponent } from '../../_components/index';
import {
  HomePageModel
} from '../../_models';
import { Globals } from 'src/app/globals';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
})
export class HomeComponent implements OnInit {

  title: string = 'Louisiane - Home';
  menuList = null;
  menuLoading = true;
  homepageCMS: HomePageModel;

  urlStorageCMS: string;
  @ViewChild('homeCMS') private homeCMS: HeroSearchComponent;

  constructor(
    private titleService: Title, 
    private menuDishService: MenuDishService,
    private help: HelpService,
    private cms: CmsService,
    private globals: Globals
  ) { 
      titleService.setTitle(this.title);
      this.urlStorageCMS = this.cms.getMediaUrl();
  }

  ngOnInit() {
    this.cms.getHompage().pipe(first()).subscribe(
      response => {
        if (response) {
          if (response.status_code === 200) {
            this.homepageCMS =  response.result;
          } else {
            console.log(response.message)
          }
        }
      },
      error => {
        console.log(error)
      }
    )
    

    this.menuDishService.getListMenuByID()
    .pipe(first()).subscribe(
      response => {
        if(response.status_code === 200 && response.result.data !== null){
          this.menuList = response.result.menu;
          this.menuLoading = false;
        }else{  
          console.log('There are no menus in this partner');
          console.log(response);
        }
      },
      error =>{
        console.log(error);
      }
    );
  }
}
