import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd, ActivatedRoute } from '@angular/router';
import { first, filter  } from 'rxjs/operators';
import { CmsService } from 'src/app/_services';

@Component({
  selector: 'app-news-detail',
  templateUrl: './news-detail.component.html',
})
export class NewsDetailComponent implements OnInit {

  newsDetailPageCms: any;
  newsSidebarCms: any;

  slug: any;
  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private cmsService: CmsService
   ) {
      this.router.events.pipe(
        filter(event => event instanceof NavigationEnd)
      ).subscribe(() => {
        this.slug = this.activatedRoute.snapshot.paramMap.get('slug');
        this.callData();
      });
   }

  ngOnInit() {
  }

  callData() {
    this.cmsService.getNewsDetailPage(this.slug).pipe(first()).subscribe(
      response => {
        if (response) {
          if(response.status_code === 200) {
            this.newsDetailPageCms = response.result;
          } else {
            alert(response.message)
          }
        } else {
          console.log('No response');
        }
      },
      error => {
        console.log(error)
      }
    );
    this.cmsService.getNewsSidebar(this.slug).pipe(first()).subscribe(
      response => {
        if (response) {
          if(response.status_code === 200) {
            this.newsSidebarCms = response.result;
          } else {
            alert(response.message)
          }
        } else {
          console.log('No response');
        }
      },
      error => {
        console.log(error)
      }
    );
  }

  gotoNewsPage() {
    this.router.navigate(['/news']);
  }

}
