import { Component, OnInit, Input, ViewChild } from '@angular/core';
import {MenuDishService, HelpService, AuthService, OrderService, IndexedDBService, CmsService} from '../../_services';
import { first, filter  } from 'rxjs/operators';
import {ActivatedRoute, Router, NavigationEnd } from '@angular/router';
import { ACTION_LOADING_SHOW, ACTION_LOADING_HIDE, ACTION_UPDATEYOURORDER, ACTION_PAYMENT_STEP_0} from '../../store/actions/index';
import {OptionsOfDishComponent} from '../../_components/index';
@Component({
  selector: 'app-dishs',
  templateUrl: './dishs.component.html',
})
export class DishsComponent implements OnInit {

  dishs = [];
  menuList = [];
  currentMenuName: string;
  menuDropdown = false;
  popup = false;
  popupImage: string;
  popupTitle: string;
  popupDescription: string;
  addon: any;
  dish: any;
  totalPrice: number;
  totalPriceAddons = 0;
  isNoDish = false;
  popupAddon = false;
  dishLoading = true;
  
  dishsPageCMS: any;
  meneID: number;
  @Input() searchText: string;

  @ViewChild('addonsPopup') private addonsPopup: OptionsOfDishComponent;

  constructor(
    private menuDishService: MenuDishService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private help: HelpService,
    private auth: AuthService,
    private orderService: OrderService,
    private indexedDBService: IndexedDBService,
    private cmsService:CmsService
  ) {
    this.indexedDBService.initObjectStore();

    this.router.events.pipe(
      filter(event => event instanceof NavigationEnd)
    ).subscribe(() => {
      this.meneID = +this.activatedRoute.snapshot.paramMap.get('id');
      this.reInit();
    });
   }


  ngOnInit() {
    this.reInit();

    this.cmsService.getDishsPage().pipe(first()).subscribe(
      response => {
        if (response) {
          if(response.status_code === 200) {
            this.dishsPageCMS = response.result;
          } else {
            alert(response.message)
          }
        } else {
          console.log('No response');
        }
      },
      error => {
        console.log(error)
      }
    );

  }
  reInit() {
    if(this.searchText){
      this.callDataSearchPage(this.searchText);
    } else {
      this.callData(this.meneID);
    }
  }
  callData(id) {
    this.dishLoading = true;
    this.dishs = [];
    this.menuDishService.getListDishByMenuID(id).pipe(first()).subscribe(
      response => {
        if(response && response.status_code === 200) {
          if(response.result !== []){
            this.dishs = response.result;

            if (this.dishs.length === 0) {
              this.isNoDish = true;
            } else {
              this.isNoDish = false;
            }

            this.dishLoading = false;
          } else {
            console.log('There is no dish');
          }
        } else {
          console.log(response)
        }
      },
      error => {
        console.log(error)
      }
    )
    this.menuDishService.getListMenuByID().subscribe(
      response => {
        if(response && response.status_code === 200) {
          if(response.result.menu !== []){
            this.menuList = response.result.menu;
            for(let menu of this.menuList){
              if(id === menu.id) {
                this.currentMenuName = menu.name;
              }
            }
            this.dishLoading = false;

          } else {
            console.log('There is no menu');
          }
        } else {
          console.log(response)
        }
      },
      error => {
        console.log(error)
      }
    )
  }

  callDataSearchPage(searchText) {
    this.dishLoading = true;
    this.dishs = [];
    this.menuDishService.getListDishBySearchText(searchText).pipe(first()).subscribe(
      response => {
        if(response && response.status_code === 200) {
          if(response.result !== []){
            this.dishs = response.result.data;
            if (this.dishs.length === 0) {
              this.isNoDish = true;
            } else {
              this.isNoDish = false;
            }

            this.dishLoading = false;
          } else {
            console.log('There is no dish');
          }
        } else {
          console.log(response)
        }
      },
      error => {
        console.log(error)
      }
    )
  }

  onOpen(e) {
    this.popupImage = e.image;
    this.popupTitle = e.name;
    this.popupDescription = e.description;
    this.popup = true;
  }
  close(e){
    if(this.menuDropdown){
      this.menuDropdown = false;
    }
   
  }
  closePopup(e){
    if(this.popup === true && e.target.id === 'ls-popup-img'){
        this.popup = false;
    }
  }
  menuDropdownFunc() {
    this.menuDropdown = !this.menuDropdown;
  }

  addToCartFunc(dish) {
    if(this.auth.isAuth()) {
      this.addon = this.orderService.getAddons(dish.id);
      if(this.addon) {
        this.popupAddon = true;
        this.dish = dish;
        // calc total price when init
        this.totalPrice = this.orderService.getPriceDish(this.dish);
        if(this.addon) {
          this.totalPriceAddons = this.orderService.getBeginAddonPrice(this.addon);
        }
        this.totalPrice += this.totalPriceAddons;
        this.addonsPopup.reInit();
      } else {

        let orderDish = {
          dishID: null,
          dishName: null,
          quantity: null,
          pricePerDish: null,
          price: null,
          addons: null                
        }
        orderDish.dishID = dish.id;
        orderDish.dishName = dish.name;
        orderDish.quantity = 1;
        orderDish.pricePerDish = dish.price;
        orderDish.price = dish.price;
        orderDish.addons = null;
        if(orderDish) {
          // this.indexedDBService.initObjectStore();
          this.indexedDBService.addNewRow(orderDish);  
          let totalPriceCart = 0;
          this.indexedDBService.readData().then( (result) => {
            result.forEach(item => {
              totalPriceCart += item.price;
            });
            totalPriceCart = this.roundNumber(totalPriceCart);
            this.orderService.setTotalPriceCart(totalPriceCart);
            this.orderService.updateYourOrderState({
              action: ACTION_UPDATEYOURORDER,
              payload: {
                deliveryFee: this.orderService.getDeleveryFee() ? this.orderService.getDeleveryFee() : 0,
                distance: this.orderService.getDistance() ? this.orderService.getDeleveryFee() : 0,
                totalPriceCart: this.orderService.getTotalPriceCart()
              }
            });
            this.popupAddon = false;        
          })
        }
      }

     
    } else{
      this.router.navigate(['sign-in']);
    }


  }
  roundNumber(num) {
    return Math.round(num * 100) / 100
  }
  closeAddonPopup(e){
    this.popupAddon = e;
  }

}
