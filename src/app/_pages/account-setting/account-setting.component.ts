import { Component, OnInit  } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { AuthService, UserService, HelpService } from '../../_services/index';
import { first, filter } from 'rxjs/operators';
import {ACTION_LOGIN, ACTION_LOADING_SHOW, ACTION_LOGOUT, ACTION_LOADING_HIDE} from '../../store/actions/index';
import {ActivatedRoute, Router, NavigationEnd } from '@angular/router';

@Component({
  selector: 'app-account-setting',
  templateUrl: './account-setting.component.html',
})
export class AccountSettingComponent implements OnInit {

  editProfileForm: FormGroup;
  currentUser: any;
  image: string;
  submitted = false;
  loading = false;
  avatar: string;
  dob: any;
  locationError1 = false;
  locationError2 = false;
  locationError3 = false;
  location_1 = null;
  location_2 = null;
  location_3 = null;
  location_text_1 = null;
  location_text_2 = null;
  location_text_3 = null;
  public addrKeys: string[];
  public addr: object;

  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private userSerive: UserService,
    private help: HelpService,
    private router:Router
  ) { 


    this.checkUserLoggedin();



  }

  ngOnInit() {
    this.userSerive.getMyprofile().subscribe(
      response => {
        if(response) {
          if(response.status_code === 200) {
            this.userSerive.updateCurrentUser(response.result);
            this.currentUser = this.userSerive.getCurrentUser();
            this.location_1 = {
              lat: this.currentUser.user_profile.address_1_lat,
              lng: this.currentUser.user_profile.address_1_lng,
            }
            this.location_2 = {
              lat: this.currentUser.user_profile.address_2_lat,
              lng: this.currentUser.user_profile.address_2_lng,
            }
            this.location_3 = {
              lat: this.currentUser.user_profile.address_3_lat,
              lng: this.currentUser.user_profile.address_3_lng,
            }
            this.editProfileForm.controls['owallet'].setValue(this.currentUser.user_profile.wallet);  

            this.getLocationTextExist();


               // set default dob             
          }
        }
      }
    )

    // get location
    this.dob = new Date(this.currentUser.user_profile.dob).toISOString().substr(0, 10);

    this.editProfileForm = this.formBuilder.group({
      username: new FormControl(
        this.currentUser.name ? this.currentUser.name : '',
        [
          Validators.maxLength(30),
        ]
      ),

      dateofbirth: new FormControl(
        this.dob ? this.dob : '',
      ),
      email: new FormControl(
        this.currentUser.email ? this.currentUser.email : '',
        [ 
          Validators.email,
          Validators.maxLength(50),
        ]
      ),
      owallet: new FormControl(
        { value: this.currentUser.user_profile.wallet ? this.currentUser.user_profile.wallet : 0,
        disabled: true}
      ),
      phone: new FormControl(
        {value: this.currentUser.phone ? this.currentUser.phone : '', disabled: true}
      ),
      address_1: new FormControl(this.location_text_1),
      address_title_1: [ ( this.currentUser.user_profile.address_1 && this.currentUser.user_profile.address_1 !== 'null') ? this.currentUser.user_profile.address_1 : ''],
      address_2: new FormControl(this.location_text_2),
      address_title_2: [ (this.currentUser.user_profile.address_2 && this.currentUser.user_profile.address_2 !== 'null') ? this.currentUser.user_profile.address_2 : ''],
      address_3: new FormControl(this.location_text_3),
      address_title_3: [(this.currentUser.user_profile.address_3 && this.currentUser.user_profile.address_3 !== 'null') ? this.currentUser.user_profile.address_3 : ''],
  });
 



  }
  
  get f() { return this.editProfileForm.controls; }
  checkUserLoggedin = () => {
    if (localStorage.getItem('currentUser')) {
      this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    } else {
      this.currentUser = null;
    }
  }
  onSubmit() {
    this.submitted = true;
    // stop here if form is invalid
    if (this.editProfileForm.invalid 
      || this.locationError1 === true 
      || this.locationError2 === true 
      || this.locationError3 === true) {
        return;
    }
  
    this.help.updateLoadingState({
      action: ACTION_LOADING_SHOW
    });
    this.userSerive.editProfile(
      this.f.username.value,
      this.f.email.value,
      this.f.address_title_1.value,
      this.f.address_title_2.value,
      this.f.address_title_3.value,
      this.f.dateofbirth.value,
      this.location_1,
      this.location_2,
      this.location_3,
    ).pipe(first()).subscribe(
      response => {
        if(response.status_code === 200){
          this.userSerive.updateCurrentUser(response.result);
          this.currentUser = this.userSerive.getCurrentUser();
          this.help.updateLoadingState({
            action: ACTION_LOADING_HIDE
          });
          // update use profile for other component
          this.authService.updateState({
            action: ACTION_LOGIN
          });
        }

      }
    )
  }
  onFileChange(e) {
    this.help.updateLoadingState({
      action: ACTION_LOADING_SHOW
    });
    const file = e.target.files[0];
    this.userSerive.editAvatar(file)
    .pipe(first())
    .subscribe(
      response => {
        if(response.status_code === 200){
          this.userSerive.updateCurrentUser(response.result);
          this.currentUser = response.result;  
          this.help.updateLoadingState({
            action: ACTION_LOADING_HIDE
          });
          this.authService.updateState({
            action: ACTION_LOGIN
          });
  
        } else {
          console.log(response);
          return;
        }
      },
      error => {
        console.log('Can NOT send request');
        console.log(error);
      }
    )

  }

  onSearchChange1(e) {
    if(e.target.value){
      this.help.updateLoadingState({
        action: ACTION_LOADING_SHOW
      });
      this.userSerive.getLocation(e.target.value).subscribe(
        location => {
          if(location){
            this.locationError1 = false;
            this.location_1 = location;
            this.help.updateLoadingState({
              action: ACTION_LOADING_HIDE
            });
          }else{
            this.locationError1 = true;
            this.help.updateLoadingState({
              action: ACTION_LOADING_HIDE
            });
          }
        }
      )
    } else {
      this.locationError1 = false;
    }
  }

  onSearchChange2(e) {
    if(e.target.value){
      this.help.updateLoadingState({
        action: ACTION_LOADING_SHOW
      });
      this.userSerive.getLocation(e.target.value).subscribe(
        location => {
          if(location){
            this.locationError2 = false;
            this.location_2 = location;
            this.help.updateLoadingState({
              action: ACTION_LOADING_HIDE
            });
          }else{
            this.locationError2 = true;
            this.help.updateLoadingState({
              action: ACTION_LOADING_HIDE
            });
          }
        }
      )
    } else {
      this.locationError2 = false;
    }


  }
  onSearchChange3(e) {
    if(e.target.value){
      this.help.updateLoadingState({
        action: ACTION_LOADING_SHOW
      });
      this.userSerive.getLocation(e.target.value).subscribe(
        location => {
          if(location){
            this.locationError3 = false;
            this.location_3 = location;
            this.help.updateLoadingState({
              action: ACTION_LOADING_HIDE
            });
          }else{
            this.locationError3 = true;
            this.help.updateLoadingState({
              action: ACTION_LOADING_HIDE
            });
          }
        }
      )
  } else {
      this.locationError2 = false;
    }
  }


  // function get exist location
  getLocationTextExist() {
    this.userSerive.getTextFromLocation(this.currentUser.user_profile.address_1_lat,this.currentUser.user_profile.address_1_lng )
    .subscribe(
      locationText => {
        if (locationText) {
          this.location_text_1 = locationText;
          this.editProfileForm.controls['address_1'].setValue(this.location_text_1);  
        }
      }
    )
    this.userSerive.getTextFromLocation(this.currentUser.user_profile.address_2_lat,this.currentUser.user_profile.address_2_lng )
    .subscribe(
      locationText => {
        if (locationText) {
          this.location_text_2 = locationText;
          this.editProfileForm.controls['address_2'].setValue(this.location_text_2);  
        }
      }
    )
    this.userSerive.getTextFromLocation(this.currentUser.user_profile.address_3_lat,this.currentUser.user_profile.address_3_lng )
    .subscribe(
      locationText => {
        if (locationText) {
          this.location_text_3 = locationText;
          this.editProfileForm.controls['address_3'].setValue(this.location_text_3);  
        }
      }
    )
  }

}
