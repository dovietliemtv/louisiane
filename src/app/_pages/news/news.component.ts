import { Component, OnInit } from '@angular/core';
import { CmsService } from 'src/app/_services';
import { first } from 'rxjs/operators';

@Component({
  selector: 'app-news',
  templateUrl: './news.component.html',
})
export class NewsComponent implements OnInit {

  newsPageHeroBanner: any;
  newsList: any[];
  constructor(
    private cmsService: CmsService
  ) { }

  ngOnInit() {
    this.cmsService.getNewsPage().pipe(first()).subscribe(
      response => {
        if (response) {
          if (response.status_code === 200) {
            this.newsPageHeroBanner =  response.result;
            console.log(this.newsPageHeroBanner)
          } else {
            console.log(response.message)
          }
        }
      },
      error => {
        console.log(error)
      }
    )

    this.cmsService.getAllPost().pipe(first()).subscribe(
      response => {
        if (response) {
          if(response.status_code === 200) {
            this.newsList = response.result;
          } else {
            alert(response.message)
          }
        } else {
          console.log('No response');
        }
      },
      error => {
        console.log(error)
      }
    );

  }

}
