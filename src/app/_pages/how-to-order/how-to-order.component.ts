import { Component, OnInit } from '@angular/core';
import { CmsService } from 'src/app/_services';
import { first } from 'rxjs/operators';

@Component({
  selector: 'app-how-to-order',
  templateUrl: './how-to-order.component.html',
})
export class HowToOrderComponent implements OnInit {

  howToOrder: any;
  mediaUrl: string;
  constructor(
    private cmsService: CmsService,
  ) { 
    this.mediaUrl = this.cmsService.getMediaUrl();
  }

  ngOnInit() {
    this.cmsService.getHowToOrderPage().pipe(first()).subscribe(
      response => {
        if (response) {
          if (response.status_code === 200) {
            this.howToOrder = response.result;
          } else {
            alert(response.message)
            console.log(response.message)
          }
        }
      },
      error => {
        console.log(error);
      }
    )
  }

}
