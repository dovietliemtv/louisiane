import { Component, OnInit } from '@angular/core';
import { CmsService } from 'src/app/_services';
import { first } from 'rxjs/operators';

@Component({
  selector: 'app-term-of-use',
  templateUrl: './term-of-use.component.html',
})
export class TermOfUseComponent implements OnInit {

  termOfUsePage: any;
  constructor(
    private cmsService: CmsService
  ) { }

  ngOnInit() {

    this.cmsService.getTermOfUsePage().pipe(first()).subscribe(
      response => {
        if (response) {
          if(response.status_code === 200) {
            this.termOfUsePage = response.result;
            console.log(this.termOfUsePage)
          } else {
            alert(response.message)
            console.log(response.message)
          }
        } else {
          console.log('No response');
        }
      },
      error => {
        console.log(error)
      }
    );


  }

}
