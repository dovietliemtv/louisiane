import { Component, OnInit } from '@angular/core';
import { CmsService } from 'src/app/_services';
import { first } from 'rxjs/operators';

@Component({
  selector: 'app-help',
  templateUrl: './help.component.html',
})
export class HelpComponent implements OnInit {
  currentUser: any;
  helpPage: any;
  constructor(
    private cmsService:CmsService
  ) { }

  ngOnInit() {
    this.checkUserLoggedin();

    this.cmsService.getHelpPage().pipe(first()).subscribe(
      response => {
        if (response) {
          if(response.status_code === 200) {
            this.helpPage = response.result;
          } else {
            alert(response.message)
          }
        } else {
          console.log('No response');
        }
      },
      error => {
        console.log(error)
      }
    );
  }

  checkUserLoggedin = () => {
    if(localStorage.getItem('currentUser')){
      this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    }else{
      this.currentUser = null;
    }
  }
}
