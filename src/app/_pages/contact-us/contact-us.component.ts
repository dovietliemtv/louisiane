import { Component, OnInit } from '@angular/core';
import { CmsService } from 'src/app/_services';
import { first } from 'rxjs/operators';

@Component({
  selector: 'app-contact-us',
  templateUrl: './contact-us.component.html',
})
export class ContactUsComponent implements OnInit {

  contactUsPage: any;
  constructor(
    private cmsService: CmsService
  ) { }

  ngOnInit() {
    this.cmsService.getContactUsPage().pipe(first()).subscribe(
      response => {
        if (response) {
          if(response.status_code === 200) {
            this.contactUsPage = response.result;
          } else {
            alert(response.message)
            console.log(response.message)
          }
        } else {
          console.log('No response');
        }
      },
      error => {
        console.log(error)
      }
    );
    
  }

}
