import { Component, OnInit } from '@angular/core';
import { CmsService } from 'src/app/_services';
import { first } from 'rxjs/operators';

@Component({
  selector: 'app-privacy-policy',
  templateUrl: './privacy-policy.component.html',
})
export class PrivacyPolicyComponent implements OnInit {

  privacyPolicy: any;
  constructor(
    private cmsService: CmsService
  ) { }

  ngOnInit() {
    this.cmsService.getPricacyPolitcyPage().pipe(first()).subscribe(
      response => {
        if (response) {
          if(response.status_code === 200) {
            this.privacyPolicy = response.result;
          } else {
            alert(response.message)
            console.log(response.message)
          }
        } else {
          console.log('No response');
        }
      },
      error => {
        console.log(error)
      }
    );

  }

}
