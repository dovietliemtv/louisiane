import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd, ActivatedRoute } from '@angular/router';
import { first, filter  } from 'rxjs/operators';
import { CmsService } from 'src/app/_services';

@Component({
  selector: 'app-search-page',
  templateUrl: './search-page.component.html',
})
export class SearchPageComponent implements OnInit {

  searchText: string;
  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private cmsService: CmsService
  ) {

    this.router.events.pipe(
      filter(event => event instanceof NavigationEnd)
    ).subscribe(() => {
      this.searchText = this.activatedRoute.snapshot.paramMap.get('text');
    });

   }

  ngOnInit() {
  }

}
