import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Title } from '@angular/platform-browser';
import { first } from 'rxjs/operators';
import {ACTION_LOGIN, ACTION_LOADING_SHOW, ACTION_LOGOUT, ACTION_LOADING_HIDE} from '../../store/actions/index';
import {
  AuthService,
  HelpService,
  UserService
} from './../../_services/index';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
})
export class LoginComponent implements OnInit {
  inputPhoneNumberForm: FormGroup;
  verifyForm: FormGroup;
  currentUser: any;


  loading = false;


  submitted = false;
  verifysubmitted = false;
  returnUrl: string;
  defaultVal = '+84';
  title = 'Louisiane - Login';
  inputVerifyCode = false;
  errorPhoneNumber = false;
  errorVerifyCode = false;
  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private titleService: Title,
    private authService: AuthService,
    private userService: UserService,
    private help: HelpService
  ) {

    // set title for this page

    titleService.setTitle(this.title);
    this.currentUser = this.userService.getCurrentUser();

   }

  ngOnInit() {
    this.inputPhoneNumberForm = this.formBuilder.group({
      phonenumber: ['', Validators.required]
    });

   this.verifyForm = this.formBuilder.group({
    verifynumber: ['', Validators.required]
    });
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
  }
  get f() { return this.inputPhoneNumberForm.controls; }
  get v() { return this.verifyForm.controls; }

  onSubmit = () => {
    this.submitted = true;
    // stop here if form is invalid
    if (this.inputPhoneNumberForm.invalid) {
        return;
    }

    this.help.updateLoadingState({
      action: ACTION_LOADING_SHOW
    });
// Sent sms
    this.loading = true;
    this.authService.sendSMS(this.f.phonenumber.value)
    .pipe(first())
    .subscribe(
      response => {
        this.loading = false;
        this.help.updateLoadingState({
          action: ACTION_LOADING_HIDE
        });
        if (response.status_code === 200) {
          this.inputVerifyCode = true;
          this.errorPhoneNumber = false;
        } else {
          this.errorPhoneNumber = true;
        }
      },
      error => {
        console.log(error);
      }
    );
  }
  // resend SMS
  resendSms = (e) => {
    this.help.updateLoadingState({
      action: ACTION_LOADING_SHOW
    });
    e.preventDefault();
    this.authService.sendSMS(this.f.phonenumber.value)
    .pipe(first())
    .subscribe(
      response => {
        // this.loading = false;
        this.help.updateLoadingState({
          action: ACTION_LOADING_HIDE
        });
        if (response.status_code === 200) {
          this.inputVerifyCode = true;
          this.errorPhoneNumber = false;
        } else {
          this.errorPhoneNumber = true;
        }
      },
      error => {
        console.log(error);
      }
    );
  }

  onVerifySubmit = () => {
    this.verifysubmitted = true;
    // this.loading = true;
    this.help.updateLoadingState({
      action: ACTION_LOADING_SHOW
    });
    // stop here if form is invalid
    if (this.verifyForm.invalid) {
        this.help.updateLoadingState({
          action: ACTION_LOADING_HIDE
        });
        this.errorVerifyCode = false;
        return;
    }
    this.authService.verifySMS(this.v.verifynumber.value)
    .pipe(first())
    .subscribe(
      response => {
        // this.loading = false;
        this.help.updateLoadingState({
          action: ACTION_LOADING_HIDE
        });
        if (response && response.status_code === 200) {
          localStorage.setItem('currentUser', JSON.stringify(response.result.user));
          localStorage.setItem('token', response.result.token);

          this.router.navigate([this.returnUrl]);
          this.errorVerifyCode = false;
          this.authService.updateState({
            action: ACTION_LOGIN
          });
        } else {
          this.errorVerifyCode = true;
        }
      },
      error => {
        // this.loading = false;
        this.help.updateLoadingState({
          action: ACTION_LOADING_HIDE
        });
      }
    );
  }
}
