export const ACTION_LOGOUT = 'LOGOUT';
export const ACTION_LOGIN = 'LOGIN';
export const ACTION_LOADING_SHOW = 'LOADING';
export const ACTION_LOADING_HIDE = 'HIDE_LOADING';
export const ACTION_UPDATEYOURORDER = 'UPDATE_YOUR_ORDER';

export const ACTION_PAYMENT_STEP_0 = 'PAYMENT_STEP_0';
export const ACTION_PAYMENT_STEP_1 = 'PAYMENT_STEP_1';
export const ACTION_PAYMENT_STEP_2 = 'PAYMENT_STEP_2';
export const ACTION_PAYMENT_STEP_3 = 'PAYMENT_STEP_3';