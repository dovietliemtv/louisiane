
import {
    reducer,
    AppReducerState,
} from './appReducer';
import {
    reducerLoading,
    AppLoadingReducerState,
} from './loadingReducer';
import {
    reducerUpdateYourOrder,
    AppUpdateYourOrderState,
} from './updateYouroOrderReducer';

import {
    paymentReducer,
    PaymentReducerState,
} from './paymentStepReducer';


import {ActionReducerMap } from '@ngrx/store';

interface AppState {
    loginReducer: AppReducerState;
    appLoadingReducer: AppLoadingReducerState;
    updateYourOrderReducer: AppUpdateYourOrderState;
    paymentReducer: PaymentReducerState;

}
export const reducers: ActionReducerMap<AppState> = {
    loginReducer: reducer,
    appLoadingReducer: reducerLoading,
    updateYourOrderReducer: reducerUpdateYourOrder,
    paymentReducer: paymentReducer,

};
