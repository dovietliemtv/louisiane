

import {
    ACTION_UPDATEYOURORDER
} from '../actions/index';

export interface AppUpdateYourOrderState {
    updateyourorder: boolean;
    deliveryInfo?: any;
    // . ..
}

const initalState: AppUpdateYourOrderState = {
    updateyourorder: false,
    deliveryInfo: {
        deliveryFee: 0,
        distance: 0,
        totalPriceCart: 0,
    }
    // . ..
};
export function reducerUpdateYourOrder(state = initalState, action): AppUpdateYourOrderState {

    switch (action.type) {
        case ACTION_UPDATEYOURORDER:
            return{
                ...state,
                updateyourorder: true,
                deliveryInfo: action.payload
            };
    }
    return state;
}
