import {
    ACTION_PAYMENT_STEP_0,
    ACTION_PAYMENT_STEP_1,
    ACTION_PAYMENT_STEP_2,
    ACTION_PAYMENT_STEP_3
} from '../actions/index';

interface CartInfo {
    dish: any,
    total_price: number,
    user_name: string,
	user_address:string,
	user_address_lat: string,
	user_address_lng:string,
	delivery_name:string,
	delivery_mobile:string,
    delivery_address:string,
    delivery_time: string,
	lat:string,
	lng:string,
	contact_info: string,
	distance: number,
	delivery_fee: number,
	note:string,
	id_main_restaurant: number,
	card_token_id: string,
	putInFrontDoor : boolean
}
export interface PaymentReducerState {
    step1: boolean,
    step2: boolean,
    step3: boolean,
    cartInfo?: CartInfo;
    // . ..
}

const initalState: PaymentReducerState = {
    step1: true,
    step2: false,
    step3: false,
    cartInfo: {
        dish: null,
        total_price: null,
        user_name: null,
	    user_address:null,
	    user_address_lat: null,
	    user_address_lng:null,
	    delivery_name:null,
	    delivery_mobile:null,
        delivery_address:null,
        delivery_time: null,
	    lat:null,
	    lng:null,
	    contact_info: null,
	    distance: null,
	    delivery_fee: null,
	    note:null,
	    id_main_restaurant: null,
	    card_token_id: null,
        putInFrontDoor : false,
        
    }
    // . ..
};


export function paymentReducer(state = initalState, action): PaymentReducerState {
    switch (action.type) {
        case ACTION_PAYMENT_STEP_0:
            return{
                ...state,
                step1: false,
                step2: false,
                step3: false,
            };
        case ACTION_PAYMENT_STEP_1:
            return{
                ...state,
                step1: true,
                step2: false,
                step3: false,
                cartInfo: action.payload
            };
        case ACTION_PAYMENT_STEP_2:
            return{
                ...state,
                step1: false,
                step2: true,
                step3: false,
                cartInfo: action.payload

            };
        case ACTION_PAYMENT_STEP_3:
            return{
                ...state,
                step1: false,
                step2: false,
                step3: true,
                cartInfo: action.payload
            };    
    }
    return state;
}