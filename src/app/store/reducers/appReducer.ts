

import {
    ACTION_LOGOUT,
    ACTION_LOGIN,
} from '../actions/index';

export interface AppReducerState {
    login: boolean;
    // . ..
}

const initalState: AppReducerState = {
    login: false,
    // . ..
};


export function reducer(state = initalState, action): AppReducerState {
    switch (action.type) {
        case ACTION_LOGOUT:
            return{
                ...state,
                login: false,
            };
        case ACTION_LOGIN:
            return{
                ...state,
                login: true,
            };
    }
    return state;
}