

import {
    ACTION_LOADING_SHOW,
    ACTION_LOADING_HIDE
} from '../actions/index';

export interface AppLoadingReducerState {
    loading: boolean;
    // . ..
}

const initalState: AppLoadingReducerState = {
    loading: false,
    // . ..
};
export function reducerLoading(state = initalState, action): AppLoadingReducerState {

    switch (action.type) {
        case ACTION_LOADING_HIDE:
            return{
                ...state,
                loading: false
            };
        case ACTION_LOADING_SHOW:
            return{
                ...state,
                loading: true
        };
    }
    return state;
}
